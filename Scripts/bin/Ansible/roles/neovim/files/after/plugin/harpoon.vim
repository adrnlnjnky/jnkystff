" Harpoon config
"

nnoremap <Leader>, :lua require("harpoon.mark").add_file()<CR>
nnoremap <Leader>m :lua require("harpoon.ui").toggle_quick_menu()<CR>

nnoremap <Leader>tf :lua require("harpoon.ui").nav_file(1)<CR>
nnoremap <Leader>td :lua require("harpoon.ui").nav_file(2)<CR>
nnoremap <Leader>ts :lua require("harpoon.ui").nav_file(3)<CR>
nnoremap <Leader>ta :lua require("harpoon.ui").nav_file(4)<CR>

