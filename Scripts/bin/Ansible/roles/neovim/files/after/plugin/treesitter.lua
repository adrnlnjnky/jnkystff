require'nvim-treesitter.configs'.setup {
  ensure_installed = { "elixir", "eex", "lua" },
  sync_install = false,
  ignore_install = { "javascript", "typescript" },
  -- highlight default TSModuleInfoGood guifg=LightGreen gui=bold,
  highlight = {
    enable = true,
    disable = {},
  },
  incremental_selection = { enable = true },
  textobjects = { enable = true },
  indent = {
    enable = true
  },
}


local ts_utils = require 'nvim-treesitter.ts_utils'

