-- ###############################################################################
-- ###############################################################################
-- ###                                                                         ###
-- ###                            NVIM CONFIGURATION:                          ###
-- ###                                 GLOBALS:                                ###
-- ###                   Global variables available to all files               ###
-- ###                                                                         ###
-- ###############################################################################
-- ###############################################################################

M = {}

local ok, plenary_reload = pcall(require, "plenary.reload")
if not ok then
  reloader = require
else
  reloader = plenary_reload.reload_module
end

-- FUNCTIONS FOR EVERYONE
-- *******************************************

function Capitalize(str)
  return (str:gsub("^%l", string.upper))
end

function Split(snip, delimiter)
  local result = {}
  for match in (snip .. delimiter):gmatch("(.-)" .. delimiter) do
    table.insert(result, match)
  end
  return result
end

local function bash(_, _, command)
  local file = io.popen(command, "r")
  local res = {}
  for line in file:lines() do
    table.insert(res, line)
  end
  return res
end

function Split(snip, delimiter)
  local result = {}
  for match in (snip .. delimiter):gmatch("(.-)" .. delimiter) do
    table.insert(result, match)
  end
  return result
end

P = function(v)
  print(vim.inspect(v))
  return v
end

RELOAD = function(...)
  return reloader(...)
end

R = function(name)
  RELOAD(name)
  return require(name)
end

-- USER IDENTIFICATION
-- *******************************************
local path_table = bash(bash, {}, 'realpath " "')
local path = path_table[1]
local part = Split(path, "/")

User = part[3]
-- User = "adrnlnjnky"
-- User = "tpeltier"

-- End USER FINDING
-- ***********************************************

-- return P

-- return User
-- return M
