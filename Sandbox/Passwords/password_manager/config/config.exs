import Config

config :password_manager,
  file_loc: "$HOME/.secrets/passwords.txt"

import_config("#{config_env()}.exs")
