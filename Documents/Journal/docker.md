---
title: Docker Journal
author: Tom Peltier
date: Sun Jul  9 09:27:53 PM EDT 2023
date_Create: 2023-05-14 00:02:20
tags: [docker, container, containers, journal, journals]
query: true
---

# Docker Knowledge Journal

## Dockerfile

$ text based file
$ no extension
$ Used to build a container image.

```Dockerfile

        # syntax=docker/dockerfile:1
           
        FROM node:18-alpine
        WORKDIR /app
        COPY . .
        RUN yarn install --production
        CMD [`node`, `src/index.js`]
        EXPOSE 3000

```

```bash

        docker build -t getting-started .

```

$ docker build --> uses 'Dockerfile' as instructions to build an container.
$ -t --> flag to give it a human readable form 'getting-started'
$ . --> tells build to use the Dockerfile in the current directory

Lets start the container.

```sh

        docker run -dp 3000:3000 getting-started

```

$ -d --> flag to run detached
    * without this you cant access the app.
$ -p --> to create the mapping between 3000 and containers port 3000   

find it: http;//localhost;3000

Now lets update this app.
    FILE: src/static/js/app.js
        * line 56 make the following switch 

```javascript
        <p className="text-center"> No items yet Add one above </p>

Now the container has to be rebuilt and redeployed. 
    (don't forget to stop the running container.)

```sh

        docker stop <container-id>

        docker rm <container-id>
        
        docker build -t getting-started .

        docker run -dp 3000:3000 getting-started

```




