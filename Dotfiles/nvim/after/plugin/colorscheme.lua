-- ###############################################################################
-- ###############################################################################
-- ###                                                                         ###
-- ###                               CONFIGURATION:                            ###
-- ###                                COLORSCHEME                              ###
-- ###                       set the colorscheme here.                         ###
-- ###                                                                         ###
-- ###############################################################################
-- ###############################################################################

-- require("kanagawa").load("wave")

-- vim.cmd(":colorscheme rose-pine-dawn")
-- vim.cmd(":colorscheme oceanic_material")
-- vim.cmd(":colorscheme tokyonight")
-- vim.cmd(":colorscheme afterglow")
-- vim.cmd(":colorscheme catppuccin")
vim.cmd(":colorscheme catppuccin-macchiato")
-- vim.cmd(":colorscheme habamax")
-- vim.cmd(":colorscheme melange")
-- vim.cmd(":colorscheme wildcharm")
-- vim.cmd(":colorscheme zaibatsu")
