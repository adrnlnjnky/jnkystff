defmodule PasswordManager.MixProject do
  use Mix.Project

  def project do
    [
      app: :password_manager,
      version: "0.1.0",
      elixir: "~> 1.14",
      start_permanent: Mix.env() == :prod,
      deps: deps(),

      # Docs 
      name: "Password Manager",
      source_url: "you_wish",
      docs: [
        main: "PasswordManager",
        logo: "http://LINK_TO_A_LOGO",
        extras: ["README.md"]
      ]
    ]
  end



  # Run "mix help compile.app" to learn about applications.
  def application do
    [
      extra_applications: [:logger]
    ]
  end

  # Run "mix help deps" to learn about dependencies.
  defp deps do
    [
      {:credo, "~> 1.7", only: [:dev, :test], runtime: false},
      {:ex_doc, "~> 0.27", only: :dev, runtime: false}
    ]
  end
end
