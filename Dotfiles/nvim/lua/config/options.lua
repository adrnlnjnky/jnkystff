-- #############################################################################
-- #############################################################################
-- ###                                                                       ###
-- ###                           NVIM CONFIGURATION:                         ###
-- ###                                OPTIONS:                               ###
-- ###                         Custom options settings                       ###
-- ###                                                                       ###
-- #############################################################################
-- #############################################################################

local opt = vim.opt

-- new

-- set vertical bar at 80 columns
opt.cc = "80"

-- set.obsession.status = true

-- set -g @resurrect-strategy-nvim 'session'
-- set -g @resurrect-strategy-nvim 'session'

-- opt.scrolloff = 80
opt.scrolloff = 999

opt.termguicolors = true

opt.pumblend = 17
opt.wildmode = "longest:full"
opt.wildoptions = "pum"

opt.cmdheight = 1

opt.showmode = false
opt.showcmd = true
opt.hidden = true
opt.lazyredraw = false
opt.list = false

opt.swapfile = false
opt.linebreak = true

opt.hlsearch = true

opt.cursorcolumn = false
opt.cursorline = false

opt.relativenumber = true
opt.number = true

opt.spell = true
-- opt.spelllang = 'en-us'

-- opt.textwidth = 120
opt.textwidth = 80
opt.wrap = false

-- tab stuff
opt.autoindent = true
opt.cindent = true
opt.tabstop = 2
opt.shiftwidth = 4
opt.softtabstop = 4
opt.expandtab = true

opt.breakindent = true

-- Searching
opt.incsearch = true
opt.ignorecase = true
opt.smartcase = true
opt.showmatch = true
opt.hlsearch = true

-- splits
opt.equalalways = false
opt.splitright = true
opt.splitbelow = true

opt.updatetime = 1000

opt.belloff = "all"

opt.laststatus = 1

opt.clipboard = "unnamedplus"
-- opt.clipboard = true
opt.emoji = true
-- opt.formatoptions = false
