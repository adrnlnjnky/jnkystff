---
title: Tom Peltiers Blog
author: Tom Peltier
date_Modified: 2023-04-20 20:03:31
date_Create: 2023-04-20 20:03:31
query: true
---

## Command Line Stuff

For those of you who prefer to work from the command line I came across this go app that lets your work with Jira from the command line. I just found this so I throught I'd bring you along as I learn about Go-Jira.

It looks like Go-Jira works with api tokens, so I went to my Jira account and found the api tokens and created a token.  I named it Go-Jira.  The dbring you along as I learn about Go-Jira.

It looks like Go-Jira works with api tokens, so I went to my Jira account and found the api tokens and created a token. The documentation recommends either adding this to your keyring or using the pass/gopass environment variable.  

This lead me to more Jira documentation about using the key.  
    *. (Adding API-Token)[https://support.atlassian.com/atlassian-account/docs/manage-api-tokens-for-your-atlassian-account/]
    *. (Jira REST API examples)[developer.atlassian.com/server/jira/platform/jira-rest-api-examples/]

The Examples show how to use curl to push commands such as creating an issue or a subtask.  Perhaps this option could work well in combination with scripting.  For now I'm going to keep exploring this Go-Jira app.

After quickly looking over the documents I hopped back to the top of the Git page and started with the install
process. Turns out that go-jira is in the asdf version control software so that is how I'll be installing.  

Now for some setup.  The documentation explains that this thing has a complicated hierachy structure.  This is done so that when you switch projects go-jira will detect the change and pull down the correct yaml files.  Yes go-jira uses yaml for configuration.

Go-jira is looking for .jira.d files, and loads in a very specific manor. Then it will load <command>.yml files in that folder. Folders closer to the source have precedence and command line arguments have top precedence.
    Example: jira.d/list.yaml
```yaml
        project: foo
```

By setting up a .jira.d folder in your home directory allows for top level info like the proper endpoint.
```yaml
        endpoint: https://jira.breezeline.com


    *. you can use the -u flag to set your username.


The docs show options for dynamic seting in scripts and overriding settings and cool things like this but these feel like advanced options that I'm going to skip over for now and move to authorization.

( Man this documentation is thick and a real pain to get through.  This is going to take me awhile and I feel like at
this point I just want to write an my own app in Elixir.)

I'm almost through this though so I'll finish up with authentication add the a couple tepmlates, write some config
files and see if I can get this working.  Then maybe I'll add an Elixir project to my side project lists and look at
building an elixir app maybe using keycloak to set up some the authentication.






With that out of the way lets try giving it a trial run.






