<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Mermaid Practice</title>
    <link rel="stylesheet" href="./style.css">
    <link rel="icon" href="./favicon.ico" type="image/x-icon">
</head>

<body>
    <main>
        <h1>Murmaid Practice</h1>

        <pre class="mermaid">
            <!-- <INSERT Graph data here> -->
            graph TD  <!-- READ TOP DOWN -->
            {{_cursor_}}

        </pre>


        <script type="module">
            import mermaid from 'https://cdn.jsdelivr.net/npm/mermaid@10/dist/mermaid.esm.min.mjs';
            mermaid.initialize({startOnLoad: true});
        </script>
    </main>
</body>

</html>
