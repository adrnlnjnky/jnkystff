-- ###############################################################################
-- ###############################################################################
-- ###                                                                         ###
-- ###                            NVIM CONFIGURATION:                          ###
-- ###                               COLORSCHEMES                              ###
-- ###                                  SONOKAI                                ###
-- ###                          Lots of red and yellows                        ###
-- ###                                                                         ###
-- ###############################################################################
-- ###############################################################################
return {
  { "sainnhe/sonokai" },
}
