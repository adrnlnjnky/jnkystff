#!usr/bin/env fish
set -e
# date: Sat Apr 15 10:20:44 AM EDT 2023

#!/bin/bash

version="0.1.0"
FILE=$2
USAGE="Usage: $0 [OPTION]... <ANYTHING ELSE>

-v --verbose    verbose mode
-h --help       display this help and exit
"

case $1 in
-f)
	clear
	printf '%s\n' "$USAGE" || exit1
	exit
	;;
--help)
	clear
	printf '%s\n' "$USAGE" || exit1
	exit
	;;
esac

{{_cursor_}}

exec flags_sample.sh -cd "$@"
