-- ###############################################################################
-- ###############################################################################
-- ###                                                                         ###
-- ###                            NVIM CONFIGURATION:                          ###
-- ###                                  LSP_SAGA                               ###
-- ###                               Some LSP sugar                            ###
-- ###                                                                         ###
-- ###############################################################################
-- ###############################################################################

return {
  {
    "glepnir/lspsaga.nvim",

    keys = {
      {
        "<leader>cF",
        "<cmd>Lspsaga lsp_finder<CR>",
        desc = "LspSaga: Finder",
      },
      {
        "<leader>cl",
        desc = "LspSaga: Code-->",
      },
      {
        "<leader>cla",
        "<CMD>Lspsaga code_action<CR>",
        desc = "LspSaga: Code Action",
      },
      {
        "<leader>xr",
        "<CMD>Lspsaga rename<CR>",
        desc = "LspSaga: Rename all in file",
      },
      {
        "<leader>xR",
        "<CMD>Lspsaga rename ++project<CR>",
        desc = "LspSaga: Rename all in selected project files",
      },
      {
        "<leader>cp",
        "<CMD>Lspsaga peek_definition<CR>",
        desc = "Edit file containing definition in floating window",
      },
      {
        "<leader>ct",
        "<CMD>Lspsaga peek_type_definition<CR>",
        desc = "Edit file containing type definition in floating window",
      },
      {
        "<leader>fd",
        "<CMD>Lspsaga goto_definition<CR>",
        desc = "Edit file containing definition in floating window",
      },
      {
        "<leader>fty",
        "<CMD>Lspsaga goto_type_definition<CR>",
        desc = "Edit file containing type definition in floating window",
      },
      {
        "<leader>xd",
        "<CMD>Lspsaga show_line_diagnostics<CR>",
        desc = "LspSaga: Show bffer diagnostics",
      },
      {
        "<leader>xd",
        "<CMD>Lspsaga show_buffer_diagnostics<CR>",
        desc = "LspSaga: Show buffer diagnostics",
      },
      {
        "<leader>xw",
        "<CMD>Lspsaga show_workspace_diagnostics<CR>",
        desc = "LspSaga: Show buffer diagnostics",
      },
      {
        "<leader>xc",
        "<CMD>Lspsaga show_cursor<CR>",
        desc = "LspSaga: Show buffer diagnostics",
      },
    },

    opts = {
      event = "lspAttach",
      config = function()
        require("lspsaga").setup({})
      end,
      dependencies = {
        { "nvim-tree/nvim-web-devicons" },
        { "nvim-treesitter/nvim-treesitter" },
      },
      finder = {
        max_height = 0.5,
        min_width = 30,
        force_max_height = false,
        keys = {
          jump_to = "p",
          expand_or_jump = "o",
          vsplit = "s",
          split = "i",
          tabe = "t",
          tabnew = "r",
          quit = { "q", "<ESC>" },
          close_in_preview = "<ESC>",
        },
      },
    },
    code_action = {
      num_shortcut = true,
      show_server_name = false,
      extend_gitsigns = true,
      keys = {
        -- string | table type
        quit = "q",
        exec = "<CR>",
      },
    },
    lightbulb = {
      enable = true,
      enable_in_insert = true,
      sign = true,
      sign_priority = 40,
      virtual_text = true,
    },
    diagnostic = {
      on_insert = false,
      on_insert_follow = false,
      insert_winblend = 0,
      show_code_action = true,
      show_source = true,
      jump_num_shortcut = true,
      max_width = 0.7,
      max_height = 0.6,
      max_show_width = 0.9,
      max_show_height = 0.6,
      text_hl_follow = true,
      border_follow = true,
      extend_relatedInformation = false,
      keys = {
        exec_action = "o",
        quit = "q",
        expand_or_jump = "<CR>",
        quit_in_show = { "q", "<ESC>" },
      },
      callhierarchy = {
        show_detail = false,
        keys = {
          edit = "e",
          vsplit = "s",
          split = "i",
          tabe = "t",
          jump = "o",
          quit = "q",
          expand_collapse = "u",
        },
      },
      rename = {
        quit = "<C-c>",
        exec = "<CR>",
        mark = "x",
        confirm = "<CR>",
        in_select = true,
      },
      beacon = {
        enable = true,
        frequency = 7,
      },
      ui = {
        -- This option only works in Neovim 0.9
        title = true,
        -- Border type can be single, double, rounded, solid, shadow.
        border = "single",
        winblend = 0,
        expand = "",
        collapse = "",
        code_action = "💡",
        incoming = " ",
        outgoing = " ",
        hover = " ",
        kind = {},
      },
    },
  },
}
