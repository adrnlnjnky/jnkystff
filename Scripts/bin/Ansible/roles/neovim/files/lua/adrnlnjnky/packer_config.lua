
return require'packer'.startup(function()

  use 'wbthomason/packer.nvim'
  use 'lewis6991/impatient.nvim'

  -- use "mhanberg/elixir.nvim"

-- and tee sitter
  use {'nvim-treesitter/nvim-treesitter', run = ':TSUpdate'}
  use 'nvim-treesitter/completion-treesitter'
  use 'nvim-treesitter/playground'
  use 'vigoux/architext.nvim'

-- completion
  -- use 'haorenW1025/completion-nvim'
  use 'kosayoda/nvim-lightbulb'
  use 'tami5/lspsaga.nvim'

  -- use 'glepnir/lspsaga.nvim', { 'branch': 'main' }

-- LSP
  use 'norcalli/nvim-terminal.lua'
  use 'neovim/nvim-lspconfig'
  use 'nvim-lua/completion-nvim'
  use 'steelsojka/completion-buffers'
  use 'voldikss/vim-floaterm'

  use 'sumneko/lua-language-server'

  -- bracket completion
  use {
    'windwp/nvim-autopairs',
    config = function() require("nvim-autopairs").setup {} end
  }

  use 'hrsh7th/cmp-buffer'
  use 'hrsh7th/cmp-path'
  use 'hrsh7th/cmp-nvim-lua'
  use 'hrsh7th/cmp-nvim-lsp'
  use 'hrsh7th/cmp-cmdline'
  use 'hrsh7th/nvim-cmp'
  -- use 'saadparwaiz1/cmp_luasnip'

  -- Snippet Stuff
  use 'L3MON4D3/LuaSnip'
  use 'saadparwaiz1/cmp_luasnip'
  use 'rafamadriz/friendly-snippets'

  -- tabnine
  use { 'tzachar/cmp-tabnine', run = './install.sh', requires = 'hrsh7th/nvim-cmp'}

  use 'kyazdani42/nvim-web-devicons'
  use {'kyazdani42/nvim-tree.lua', tag =  'nightly'}

  use {
    'folke/trouble.nvim',
    cmd = 'trouble',
    config = function()
      require('trouble').setup {
        auto_preview = false,
        auto_fold = true,
      }
    end,
  }

  use "folke/todo-comments.nvim"

  use 'mfussenegger/nvim-dap'
  use 'rcarriga/nvim-dap-ui'
  use 'theHamsta/nvim-dap-virtual-text'
  use 'nvim-telescope/telescope-dap.nvim'
  use 'mfussenegger/nvim-dap-python'

  use 'code-biscuits/nvim-biscuits'
 -- Some New STuff
 -- This one makes fancy symbols on the LSP completion popup
  use 'onsails/lspkind-nvim'
  -- This one is more info into the lsp data I think this is best for rust but may be good for diagnostics so we need to
  -- use 'nvim-lua/lsp_extensions.nvim'
  use 'vim-utils/vim-man'

  -- Git Helper
  use 'rhysd/git-messenger.vim'

  -- Hail TPope
  use 'tpope/vim-commentary'
  use 'tpope/vim-fugitive'
  use 'tpope/vim-surround'
  use 'tpope/vim-repeat'
  use 'tpope/vim-markdown'
  use 'tpope/vim-projectionist'
  use 'tpope/vim-dispatch'
  use 'tpope/vim-dadbod'
  use 'tpope/vim-unimpaired'
  use 'tpope/vim-abolish'
  use 'tpope/vim-liquid'

  use { 'kristijanhusak/vim-dadbod-completion' }
  use { 'kristijanhusak/vim-dadbod-ui' }

  -- Telescope Stuff
  use 'nvim-lua/plenary.nvim'
  use 'nvim-lua/popup.nvim'
  use 'nvim-lua/telescope.nvim'
  use 'nvim-telescope/telescope-fzy-native.nvim'

  -- Elixir Stuff
  use 'elixir-editors/vim-elixir'
  -- use 'elixir-lang/vim-elixir'
  use 'thinca/vim-ref'
  use {'awetzel/elixir.nvim',  run =  './install.sh' }

  use 'bronson/vim-trailing-whitespace'


  -- Typescript and Javascript and ...
  if false then
    use "jelera/vim-javascript-syntax"
    use "othree/javascript-libraries-syntax.vim"
    use "leafgarland/typescript-vim"
    use "peitalin/vim-jsx-typescript"

    use {"vim-script/JavaScript-Indent", ft = "javascript" }
    use {"pangloss/vim-javascript", ft = { "javascript", "html" } }

    -- Godot
    use "habamax/vim-godot"
    --
  end

  use {"prettier/vim-prettier",
  ft = { "html", "javascript", "tyescript", "typescriptreact" },
  run = "yarn install",
}


  use { 'Shougo/deoplete.nvim', run = ':UpdateRemotePlugins' }

  --# Moving between working files
  use 'ThePrimeagen/harpoon'

  -- Moving between git branches
  use 'ThePrimeagen/git-worktree.nvim'
  use 'hoob3rt/lualine.nvim'



  -- Tarraform Stuff
  use 'hashivim/vim-terraform'

  use 'prabirshrestha/asyncomplete.vim'
  use 'prabirshrestha/async.vim'
  use 'prabirshrestha/vim-lsp'
  use 'prabirshrestha/asyncomplete-lsp.vim'

  use 'mattn/vim-lsp-settings'

  -- use 'Shougo/deoplete.nvim', {'do': ':UpdateRemotePlugins' }

  -- Color Work
  use 'vim-conf-live/vimconflive2021-colorscheme'
  use 'folke/lsp-colors.nvim'
  use 'lifepillar/vim-gruvbox8'
  use 'morhetz/gruvbox'
  use 'norcalli/nvim-colorizer.lua' -- This brings me the most joy
  use 'challenger-deep-theme/vim' -- , { 'as': 'challenger-deep' }
  use 'jaredgorski/spacecamp'
  use 'whatyouhide/vim-gotham'
  use 'gilgigilgil/anderson.vim'
  use 'Mofiqul/dracula.nvim'
  -- use 'flazz/vim-colorscheme'
  use 'chriskempson/base16-vim'




end)
