-- ###############################################################################
-- ###############################################################################
-- ###                                                                         ###
-- ###                            NVICONFIGURATION:                            ###
-- ###                               KEYMAPPINGS                               ###
-- ###                Keyboard shortcuts for all sorts of commands             ###
-- ###                                                                         ###
-- ###############################################################################
-- ###############################################################################

local nmap = require("config.keymaps").nmap
local imap = require("config.keymaps").imap
local wk = require("which-key")

-- New Stuff

vim.keymap.set("n", "<leader>pv", vim.cmd.EX, { desc = "File Tree" })
-- " vim.keymap.set("n", "<leader>pv", vim.cmd.EX)

--   SAVING and SOURCING
nmap({ ",,", ":w<CR>", { silent = true, desc = "Save" } })
nmap({ ",x", ":w<CR>:so %<CR>", { silent = true, desc = "Save and Source Buffer" } })
-- nmap({ "<locallocal>x", ":w<CR>:so %<CR>", { silent = true, desc = "Save and Source Buffer" } })
nmap({ ",.", ":sav $HOME/Documents/Notes/", { silent = true, desc = "Save and Switchg/h" } })

--   Easier ESCAPING
imap({ "kj", "<Esc>", { silent = true, desc = "Exit Insert Mode" } })
imap({ "jj", "<Esc>", { silent = true, desc = "Exit Insert Mode" } })
imap({ "jk", "<Esc>", { silent = true, desc = "Exit Insert Mode" } })

imap({ "kk", "<Esc>", { silent = true, desc = "Exit Insert Mode" } })
--   Easier VIM SPLIT
nmap({ "<C-H>", "<C-W><C-H>", { silent = true, desc = "Nvim Split Right" } })
nmap({ "<C-J>", "<C-W><C-J>", { silent = true, desc = "Nvim Split Down" } })
nmap({ "<C-K>", "<C-W><C-K>", { silent = true, desc = "Nvim Split Up" } })
nmap({ "<C-L>", "<C-W><C-L>", { silent = true, desc = "Nvim Split Left" } })

-- DATE and TIME Stuff
nmap({ "<leader>d", "<esc>!!date<CR>idate: <esc>j", { desc = "Insert Date and Time" } })

-- Spelling
nmap({ ",s", "z=1<CR>", { desc = "Correct Spelling" } })

-- LINE NUMBERING
wk.register({
  ["<leader>n"] = {
    name = "+numbers",
    o = { ":rnu!<CR>", "set reletive line numbers" },
    i = { ":nu!<CR>", "set line numbers" },
  },
})

-- MODIFY PERMISSIONS
nmap({ "<leader>me", ":!chmod 700 %<CR>", { desc = "make executable and restricted" } })
-- wk.register({
--     ["<leader>c"] = {
--         name = "+code",
--         e = { ":!chmod 700 %<CR>" },
--     }
-- })

--   HIGHLIGHTING
wk.register({
  ["<leader>"] = {
    -- name = "+highlighting",
    h = { ":set cursorline!<CR>", "Toggle Highlight Line" },
    H = { ":set cursorline! cursorcolumn!<CR>", "Toggle Highlight All" },
  },
})

--  QUICKFIX LIST

nmap({
  "<leader>xm",
  function()
    vim.cmd(":cg")
    require("telescope.builtin").quickfix({})
  end,
  { desc = "Quickfix: Make and Populate" },
})

-- END
-- =====
