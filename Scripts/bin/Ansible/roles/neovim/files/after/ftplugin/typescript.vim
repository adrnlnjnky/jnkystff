let b:ale_linters = ['eslint', 'prettier', 'tsserver']
let b:ale_fixers = ['prettier']

noremap <leader>tn :!tmux send-keys -t TESTING cl Enter yarn\ test Enter<CR><CR>
