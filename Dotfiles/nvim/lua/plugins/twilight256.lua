-- ###############################################################################
-- ###############################################################################
-- ###                                                                         ###
-- ###                            NVIM CONFIGURATION:                          ###
-- ###                               COLORSCHEMES                              ###
-- ###                               TWILIGHT245                               ###
-- ###                  muted yellows and greys with  some orange              ###
-- ###                                                                         ###
-- ###############################################################################
-- ###############################################################################

return {
  { "vim-scripts/twilight256.vim" },
}
