defmodule MakeApp do
  @moduledoc """
  Documentation for `MakeApp`.
  """

  @doc """
  Hello world.

  ## Examples

      iex> MakeApp.hello()
      :world

  """
  def hello do
    :world
  end
end
