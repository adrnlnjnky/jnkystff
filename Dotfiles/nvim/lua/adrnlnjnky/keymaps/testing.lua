-- ###############################################################################
-- ###############################################################################
-- ###                                                                         ###
-- ###                            NVIM CONFIGURATION:                          ###
-- ###                                  TESTING                                ###
-- ###                Keyboard shortcuts for all sorts of commands             ###
-- ###                                                                         ###
-- ###############################################################################
-- ###############################################################################

local nmap = require("config.keymaps").nmap
local imap = require("config.keymaps").imap
local wk = require("which-key")

nmap({
  "<leader>tn",
  ":!tmux send-keys -t TESTING cl Enter mix test Enter<CR><CR>",
  { desc = "Send tests to 'TESTING' session" },
})
