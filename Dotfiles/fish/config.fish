################################################################################
################################################################################
####                                                                         ###
####                            CONFIGURATION:                               ###
####                                FISH                                     ###
####                       Friendly Interactive Shell                        ###
####                                                                         ###
################################################################################
################################################################################

# NEW STUFF
# =========
set fish_greeting

set -U FZF_CTRL_R_OPTS "--border-label=" History ' --prompt'=
set -U FZF_DEFAULT_COMMAND "df -H -E '.git'"
set -U FZF_DEFAULT_OPTS "--reverse --no-info --prompt='' --pointer='►' --marker='' --ansi --color gutter:-1,bg+:-1,header:4"
set -U FZF_TMUX_OPTS "-p --no-info --ansi --color gutter:-1,bg*:-1,header:4,separator:0,info:0,label:4,border:4,prompt:7"
set -xg KERL_BUILD_DOCS yes
set -xg KERL_BUILD_HTMDOCS yes
set -xg KERL_BUILD_MANPAGES yes
set -U ODBCSYSINI /etc/odbc.ini

# SETTINGS:
# =========
set -gx colorterm truecolor
set -gx editor nvim
set -gx virtual_env_disable_prompt true
set -gx GOPATH $HOME/.go $PATH
set -gx PATH /usr/bin $PATH
set -gx PATH /bin:/usr/bin $PATH
set -gx PATH /snap/bin $PATH
set -gx PATH $gopath/bin $PATH
set -gx PATH $HOME/.composer/vendor/bin $PATH
set -gx PATH $HOME/library/python/3.7/bin $PATH
set -gx PATH $HOME/.local/bin $PATH
set -gx PATH $HOME/.bin $PATH
set -gx PATH $HOME/.applications/bin $PATH
set -gx PATH $HOME/.local/share/nvim/mason/packages/elixir-ls/ $PATH
set -gx PATH $HOME/.asdf/shims/python3 $PATH
set -gx PATH $HOME/.cache/nvim/elixir-tools.nvim/installs/elixir-lsp/elixir-ls/tags_v0.15.1/1.15.0-25/language_server.sh $PATH
set -gx PATH $HOME/Work/Projects/dockertool/ $PATH

set -gx path $gopath/bin $home/.composer/vendor/bin $home/library/python/3.7/bin $path

# set -gx docker_buildkit 1
# set -gx compose_docker_cli_build 1

set -g fish_key_bindings fish_vi_key_bindings
set -g fish_bind_mode insert

set -g loaded_ruby_provider = 0

#     VARIABLES
# ==================
set XDG_CONFIG_HOME $HOME/.config
set XDG_CONFIG_DIRS $HOME/

# sourcing
if false ~/.completions/fish/completions/
    mkdir -p ~/.config/fish/completions; and ln -s ~/.asdf/completions/asdf.fish ~/.config/fish/completions
end

source ~/.asdf/asdf.fish

# title options
set -g theme_title_display_process yes
set -g theme_title_display_path yes
set -g theme_title_display_user yes
set -g theme_title_use_abbreviated_path yes

# prompt options
set -g theme_display_ruby yes
set -g theme_display_virtualenv yes
set -g theme_display_vagrant no
set -g theme_display_vi yes
set -g theme_display_k8s_context no # yes
set -g theme_display_user yes
set -g theme_display_hostname yes
set -g theme_show_exit_status yes
set -g theme_git_worktree_support yes
set -g theme_display_git yes
set -g theme_display_git_dirty yes
set -g theme_display_git_untracked yes
set -g theme_display_git_ahead_verbose yes
set -g theme_display_git_dirty_verbose yes
set -g theme_display_git_master_branch yes
set -g theme_display_date yes
set -g theme_display_cmd_duration yes
# set -g theme_powerline_fonts yes
set -g theme_nerd_fonts yes
set -g theme_color_scheme tokyonight-storm
# set -g theme_color_scheme solarized-dark

bind -M insert \cg aliases
bind -M insert \cg fish_greeter
bind -M insert \cg forget
bind -M insert \cg multicd
bind -M insert \cg general
# bind -M insert \cg gitlab

bind wp $HOME/Work/Projects/

# ###############################################
# ###############################################
# ##                  FUNCTIONS                ##
# ###############################################
# ###############################################

function bind_bang
    switch (commandline -t)[-1]
        case "!"
            commandline -t -- $history[1]
            commandline -f repaint
        case "*"
            commandline -i !
    end
end

function bind_dollar
    switch (commandline -t)[-1]
        case "!"
            commandline -f backward-delete-char history-token-search-backward
        case "*"
            commandline -i '$'
    end
end

function fish_user_key_bindings
    bind ! bind_bang
    bind '$' bind_dollar
end
