let g:smoking_ears_colorscheme = "gruvbox"
fun! ColorMyPencils()
    let g:gruvbox_contrast_dark = 'hard'
    if exists('+termguicolors')
        let &t_8f = "\<Esc>[38;2;%lu;%lu;%lum"
        let &t_8b = "\<Esc>[48;2;%lu;%lu;%lum"
    endif
    let g:gruvbox_invert_selection='0'

    set background=dark
    if has('nvim')
        call luaeval('vim.cmd("colorscheme " .. _A[1])', [g:smoking_ears_colorscheme])
    else
        " TODO: What the way to use g:smoking_ears_colorscheme
        colorscheme gruvbox
    endif

    highlight ColorColumn ctermbg=0 guibg=grey
    hi SignColumn guibg=none
    hi CursorLineNR guibg=None
    highlight Normal guibg=none
    " highlight LineNr guifg=#ff8659
    " highlight LineNr guifg=#aed75f
    highlight LineNr guifg=#5eacd3
    highlight netrwDir guifg=#5eacd3
    highlight qfFileName guifg=#aed75f
    hi TelescopeBorder guifg=#5eacd
endfun
call ColorMyPencils()

" Vim with me
nnoremap <leader>cmp :call ColorMyPencils()<CR>
nnoremap <leader>vwb :let g:smoking_ears_colorscheme =
"

" highlight LspDiagnosticsDefaultError guibg=#A9308B
" highlight LspDiagnosticsSignError guibg=#45A5D4
" highlight LspDiagnosticsUnderlineWarn guibg=#A77600

" highlight clear SpellBad
" highlight SpellBad gui=undercurl


" " " Joker Theme
highlight CursorLine ctermbg=21FF13 ctermfg=black
highlight CursorColumn ctermbg=9E00FF ctermfg=darkred


" highlight CursorLine ctermbg=black  ctermfg=darkred
" highlight CursorColumn ctermbg=black ctermfg=darkred
