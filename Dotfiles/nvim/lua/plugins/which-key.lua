-- ###############################################################################
-- ###############################################################################
-- ###                                                                         ###
-- ###                            NVIM CONFIGURATION:                          ###
-- ###                              NVIM-LIGHTBULD                             ###
-- ###              Shows Lightbulb when Code Action is available              ###
-- ###                                                                         ###
-- ###############################################################################
-- ###############################################################################

local wk = require("which-key")

return {
  {
    "folke/which-key.nvim",

    --   event = "VeryLazy",
    --   opts = {
    --     plugins = { spelling = true },
    --     defaults = {
    --       mode = { "n", "v" },
    --       ["<leader>i"] = { name = "+Icons" },
    --     },
    --   },
    --
    --   -- key_labels = {
    --   --   ["<tab>"] = "Tab",
    --   --   -- ["<space>"] = "Space",
    --   -- },
  },
  -- config = function(_, opts)
  --   local wk = require("which-key")
  --   wk.setup(opts)
  a, --   wk.register(opts.defaults)
  -- end,
}
