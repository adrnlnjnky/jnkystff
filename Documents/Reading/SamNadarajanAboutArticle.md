Community Leader Sam Nadarajan shares his passion for meeting locally and connecting globally
Apr 3, 2023

Today we are sharing Sam Nadarajan’s story as an online and Atlassian Community Events (ACE) Leader! From chartering the Lansing ACE chapter in the state of Michigan, to diving into the technical tunnels within the Online Community, Sam shares with us his enthusiasm for meeting fellow Atlassian users who he can grow with as a Thought Leader in the Atlassian Ecosystem.

As we spotlight Leaders in both Atlassian Community Leader programs, we hope you enjoy the journey each Leader has taken to find a community of likeminded people interested in growing their potential with Atlassian products and practices.

sam header update update.001.jpeg

Can you tell us a little bit about yourself?

How did you hear about the Atlassian Community?
It’s funny because in the past, when I did a lot of Googling, I’d interact with community.atlassian.com, but, I didn’t think much about it because I always thought it was just an online forum like Stack Overflow. However, as I started building the Atlassian practice, I wanted to find out what businesses in the area used Atlassian products and see if we could put up a Meetup Group or something just to support each other and network. Last July, I ended up reaching out to Atlassian to see how we could make it work, and that’s when I learned about Atlassian Community Events. I started from there and chartered the Lansing ACE chapter in Michigan.

Why did you decide to be a Leader in the Online and ACE programs?


What have you enjoyed about being an Online/ACE Leader?
For the online community, I have many big pluses. 
    (1) I like that it has helped improve the accuracy and succinctness of my responses. I noticed that change within a couple of months! 
    (2) It’s nice to see answers from other Leaders, and then jump on regional call with them and think “Oh snap I recognize you because you provided a really great answer to this one question from a while back!“ Having that personal touch with Community Leaders at regional calls, or at ACE has     been so neat. 
    (3) It's also super beneficial to see what people are talking about 
    online because it feeds into my ideas for ACE topics, blog posts, work proposals, etc. I try to use content from all of these different sources and push them out in different ways. (4) Lastly, I appreciate the online community for keeping me humble without making me feel bad. It expands my mind and prompts me to think, because I see there are other ways to approach a question that is different from how I would have. If you can make me think, you rank high in my books.

What I like about ACE is that it’s a new challenge. It feels like a mini startup where I can practice going out of my comfort zone by not just marketing the benefits of an ACE, but be intentional about crafting the messaging to get a bigger reach. The other really cool part is realizing those events are valuable beyond the event itself. I did a talk about advanced use cases of JQL - the kind of stuff you’d never do unless you had a special use case - and went deep into the nitty gritty of functions and business scenarios. One attendee ended up sharing the deck with her company and their jaws dropped! They got really excited and were like “What?! You can really do all of this in Jira? This is going to make reporting a lot easier and help me find information faster!“ They were amazed. As soon as I heard that feedback, I got a shot of adrenaline. Those are the moments I live for. Even though they are just moments, their staying power is long lasting and carries me through during tougher times of work.

Screen Shot 2023-03-31 at 4.55.13 PM.png

---------------------------------------------

Learn about the Community Leader Program

Explore Free, User-Led Events

---------------------------------------------

What influence has your involvement in either program had in your profession?
For my profession, it has helped me be a better consultant by exposing me to new ideas and giving me the opportunity to take the ideas in my head and iron them out in a way that’s easy to understand for people who do not have the background that I may have. That’s been huge.

For my sense of community, it feels nice to know I’m not the only one struggling. One thing I love love love about the Community Leader Program is the monthly 1:1 connections with other people in the program. As soon as I see the link, I sign up. I’ve spoken to someone in Manitoba, France, Iceland, and Argentina (which was right before the World Cup Finals!) and wow it was really cool. As we share these stories and experiences, I don’t have to explain as much what I am thinking because their heads are already nodding saying “I’m right there with you, I totally understand what you’ve been going through.” If these 1:1 connections where the only benefit of the program, that would in and of itself be worth it.

Screen Shot 2023-03-31 at 4.50.09 PM.png

What content resonates most with attendees of your ACE?
One of my ACE types is called a "Deep Dive". It's where we start with common functionality that we use on a day to day basis, and look at all the functionality available within that area. The feedback I get from these events is,"Wow, I didn't know you could do that" or "I've been using this functionality for x years and didn't realize that was there" to "Oh man as soon as I implemented some things I learned in your talk my productivity went up, satisfaction went up, and I have more time to do things that I've neglected up until now." We'll be doing more deep dives in the future.

With examples like that, I tend to take content from my previous talks and have it play into my new talks, so that it all builds on each other. I started the chapter with some foundational events that are more lecture-style in nature, and then started getting my attendees to feel like we are putting together a puzzle with each new talk that connects to what we previously spoke about and includes real world scenarios.
