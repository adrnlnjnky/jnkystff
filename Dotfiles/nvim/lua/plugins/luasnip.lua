-- ###############################################################################
-- ###############################################################################
-- ###                                                                         ###
-- ###                            NVIM CONFIGURATION:                          ###
-- ###                                  LUASNIP                                ###
-- ###                          Snippet Engine for NVIM                        ###
-- ###                                                                         ###
-- ###############################################################################
-- ###############################################################################

local ls = require("luasnip")
local types = require("luasnip.util.types")

return {
  {
    "L3MON4D3/LuaSnip",

    opts = {
      updateevents = "TextChanged,TextChangedI",
      enable_autosnippets = true,
    },
    dependencies = {
      -- require("luasnip"),
    },
    ext_opts = {
      [types.choiceNode] = {
        active = {
          virt_text = { { "<-✌️- Options", "Error" } },
        },
      },
      [types.dynamicNode] = {
        active = {
          virt_text = { { "<-🐲- Dynamic", "Error" } },
        },
      },
    },

    version = "1.*",

    build = "make install_jsregexp",
  },
}
