#!/bin/bash
# set -e

# File to move active nvim file to the Jnkystff folder and push it to git.

# SETUP
####################################

# FIXED VARIABLES
DOTFILES_DIR="$HOME/.jnkystff"
TAIL="dotfiles"
BACKUP="nvim_bak"
CONFIGS="nvim"
BREAK="##############################"

# HELPER FUNCTIONS
Choose_Folder() {
	echo "What folder would you like to save to?"
	echo "Folders:"
	cd $DOTFILES_DIR
	find . -mindepth 1 -maxdepth 1 -type d \( ! -iname ".*" \) | sed 's|^\./||g'
	echo "----------"
	printf "Choice: "
	read -r FOLDER
	echo "This is the folder: $FOLDER"
	echo ""
}

Choose_Branch() {
	echo "What branch would you like to save to?"
	echo "Branches:"
	cd $DOTFILES_DIR/$FOLDER
	find . -mindepth 1 -maxdepth 1 -type d \( ! -iname ".*" \) | sed 's|^\./||g'
	echo "----------"
	printf "Choice: "
	read BRANCH
	echo "This is the branch: $BRANCH"
	echo ""
}

# DELETE old file
Delete_Branch() {
	if ! [[ -f "$PATH/$BACKUP" ]]; then
		echo "deleting old backup files from $PATH."
		rm -r $PATH/$BACKUP
	fi
}

# USER OPTOIN VARIABLES
clear

FOLDER="test"
# Choose_Folder
# if ! [[ -d $FOLDER ]]; then
# 	echo "No Such Folder $FOLDER"
# 	Choose_Folder
# fi

BRANCH="jj"
# Choose_Branch
# if ! [[ -d $BRANCH ]]; then
# 	echo "No Such Folder $BRANCH"
# 	Choose_Branch
# fi

PATH=$DOTFILES_DIR/$FOLDER/$BRANCH/$TAIL
rm -r $PATH/$BACKUP

# # Sanity check
# echo $BREAK
# echo "This is the folder: $FOLDER"
# echo "This is the branch: $BRANCH"
# echo "This is the path:   $PATH"
# echo ""

# printf "Proceed? [y/n]: "
# read GOOD_OPTS
# if ! [ $GOOD_OPTS == "y" ]; then
# 	exit
# fi

# RUN OPERATIONS
###################################

# Delete_Branch
# # DELETE old file
# if ! [[ -f "$PATH/$BACKUP" ]]; then
# 	echo "deleting old backup files from $PATH."
# 	rm -r $PATH/$BACKUP
# fi

# MOVE current nvim to backup

# # echo "moving $configs to $path/$backup"
# mv $PATH/$CONFIGS $PATH/$BACKUP
# echo ""
#
# # COPY config files to repo location
#
# cp -r $HOME/.config/$CONFIGS $PATH/
# echo "Copied: $HOME/.config/nvim"
# echo "    To: $PATH/"
# echo ""
#
# printf "Would you like to push to the repo? [y/n] "
# read PUSH_BRANCH
#
# echo $PUSH_BRANCH
# if [[ $PUSH_BRANCH == "y" ]]; then
# 	LOCATION="${PATH,,}"
# 	# LOCATION="${DOTFIlES_DIR..}/${FOLDER,,}/${BRANCH,,}"
# 	echo "Location: $LOCATION"
# # lazygit
# fi
#
echo "end of progress."
#
# # echo "Finished, Have a nice day 😀"
