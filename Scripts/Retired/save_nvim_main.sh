#!/bin/bash

DIRECTORY="/home/adrnlnjnky/Temp/nvim/"

if [ -d "$DIRECTORY" ]; then
    echo Deleting nvim directory from Temp
    rm -rfd ~/Temp/nvim
else
    echo Directory does not exist
fi

mv "/home/adrnlnjnky/.jnkystff/main/dotfiles/nvim/" "/home/adrnlnjnky/Temp/"

cp -rfd "/home/adrnlnjnky/.config/nvim" "/home/adrnlnjnky/.jnkystff/main/dotfiles/"

