-- ###############################################################################
-- ###############################################################################
-- ###                                                                         ###
-- ###                            NVIM CONFIGURATION:                          ###
-- ###                                 KEYMAPINGS                              ###
-- ###                Keyboard shortcuts for all sorts of commands             ###
-- ###                                                                         ###
-- ###############################################################################
-- ###############################################################################

local M = {}
-- local kv = require("which-key")

-- local wk = require("which-key")
-- wk.register({
--   h = { name = "highlight" },
-- })

-- SECTION: SETUP
-- ******************************************

M.imap = function(tbl)
  vim.keymap.set("i", tbl[1], tbl[2], tbl[3])
end

M.nmap = function(tbl)
  vim.keymap.set("n", tbl[1], tbl[2], tbl[3])
end

M.vmap = function(tbl)
  vim.keymap.set("v", tbl[1], tbl[2], tbl[3])
end

M.smap = function(tbl)
  vim.keymap.set("s", tbl[1], tbl[2], tbl[3])
end

-- SECTION: OPTIONS
M.opts_n = { mode = "n", prefix = "", buffer = nil, silent = false, noremap = true, nowait = true }
M.opts_i = { mode = "i", prefix = "", buffer = nil, silent = false, noremap = true, nowait = true }
M.opts_v = { mode = "v", prefix = "", buffer = nil, silent = false, noremap = true, nowait = true }

M.opts_map_n = { mode = "n", prefix = "", buffer = nil, silent = true, noremap = true, nowait = true }
M.opts_map_i = { mode = "i", prefix = "", buffer = nil, silent = true, noremap = true, nowait = true }
M.opts_map_v = { mode = "v", prefix = "", buffer = nil, silent = true, noremap = true, nowait = true }

-- -- SECTION: Prefixes
-- kv.register({
--   i = {
--     name = "+Icon Picker",
--   },
-- })

return M
