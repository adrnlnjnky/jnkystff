---
title: NEXT_JS
author: Tom Peltier
dae_Create: 2023-05-20 16:56:04
date_Modified: 2023-05-20 16:56:04
query: true
---



# NEXT_JS Learning Journal

##  Routing

### Defining Routes
    * folders defined inside app/ are define routes
    * pages make routes publicly available

### Pages, Layouts, Templates

* pages --> makes route publicly accessible. (our "index" page)
* layouts
    * shared between pages
    * define by exporting react compontnet from a layout.js file
* templates
    * wrap pages and layouts
    * persist accross routes
    * maintain state
    * good for
        * enter/exit animation
        * useEffect and useState features
        * change default framework behavior

### Links and Navigation

#### <Link>
        * extents html <a>
            * provides prefetching
        * primary way to navigate
        * Import <Link> from next/link
        * then pass href prop to the component
        * for info
            [api-reference](https://nextjs.org/docs/app/api-reference/compenents/link)       
        * Only use when you need it <Link> instead

##### Example

```javascript

    import Link from 'next/link';

    export default function Page() {
        return <Link hreff="/dashboard">Dashboard</Link>;
    }

```

#### dynamic Links

```javascript

    import Link from 'next/link';

    export dafault function PosList({ posts }) {
        return (
        <url>
            {posts.map((post) => (
              <li key={post.id}>
                <Link href={`/blog/${post.slug}`}>{post.title}</Link>
              </li>
        ))}
    }
```

#### check active link 

```javascript
    'use client';

    import {usePathname} from 'next/navigation';
    import { Link} from 'next/link';

    export function Navigation({ navLinks }) {
    const pathname = usePathname();

    return (
      <>
       {navLinks.map((link) => {
            const isActive = pathname.startsWith(link.href);
            return (
                <>
             {navLinks.map((link) => {
             const isActive = patthname.startswith(link.href);
             return (
              <Link
               className={isActive ? 'text-blue' :
               'text-black'}
               href={link.href}
                key={link.name}
              >
              {link.name}
            </Link>
            );
          })}
        </>
      );
    })}
  );
}

```

#### useRouter Hook

* programmaticaly change routes insie client components

```javascript
    'use client';

    import { useRouter } from 'next/navigation';

    export default funtion Page() {
        const router= useRouter();

        return (
            <button type="button" onclick={() => route.push'/dashboard')}>
              Dashboard
            </button>
        );
    }

```

### Route Groups

* breaks out of standard routing behavior
* organize routes without affecting the URL structure
* Opting in specific routes segments into a layout
* Create layouts by splitting application
* created by wrapping a folders name (folerName)

### Dynamic Routes

* wrap foldername in []
* [...folderName] --> catch-all 
* [[...folderName]] --> makes it optional











    














































