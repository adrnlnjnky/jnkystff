defmodule PasswordManager.EncodePassword do
  @salt_size 20

  # TODO: check if website & email have existing password and update it if
  # requried
  def encode_pass() do
    site = IO.gets("website: ")
    password = IO.gets("password: ")

    name =
      site
      |> String.split(".", trim: true)
      |> Enum.at(1)

    hash_pass =
      password
      |> hash()
      |> salt()

    save(name, hash_pass, site)
    "password saved."
  end

  defp hash(pass_raw) do
    Base.encode64(pass_raw)
  end

  defp salt(pass_hash) do

      _salt =
        for _n <- 1..@salt_size do
          <<Enum.random(50..122)>>
        end
        |> List.to_string()
        |> Base.encode64()
    # pass_hash <> "#{salt}"

    pass_hash

  end

  defp save(name, password, site) do
    file_loc = Application.get_env(:password_manager, :file_loc)
    # file_loc = "/home/adrnlnjnky/.secrets/passwords.txt"
    file = File.read!(file_loc)
    new_file = file <> "#{name},#{password},#{site}"
    File.write!(file_loc, new_file)
  end
end

