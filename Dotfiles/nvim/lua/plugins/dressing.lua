-- ###############################################################################
-- ###############################################################################
-- ###                                                                         ###
-- ###                            NVIM CONFIGURATION:                          ###
-- ###                                 TEMPLATE                                ###
-- ###      Async templates into files  with variable and Lua functions        ###
-- ###                                                                         ###
-- ###############################################################################
-- ###############################################################################

return {
  { "stevearc/dressing.nvim", opts = {
    lazy = false,
  } },
}
