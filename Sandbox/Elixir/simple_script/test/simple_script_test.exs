defmodule SimpleScriptTest do
  use ExUnit.Case
  doctest SimpleScript

  test "greets the world" do
    assert SimpleScript.hello() == :world
  end
end
