;
/* module-key = 'com.pyxis.greenhopper.jira:gh-gadget-unsupported-browser', location = 'jira-agile-module/includes/js/gadgets/unsupported-browser.soy' */
// This file was automatically generated from unsupported-browser.soy.
// Please don't edit this file by hand.

/**
 * @fileoverview Templates in namespace GH.tpl.gadget.unsupportedBrowser.
 */

if (typeof GH == 'undefined') { var GH = {}; }
if (typeof GH.tpl == 'undefined') { GH.tpl = {}; }
if (typeof GH.tpl.gadget == 'undefined') { GH.tpl.gadget = {}; }
if (typeof GH.tpl.gadget.unsupportedBrowser == 'undefined') { GH.tpl.gadget.unsupportedBrowser = {}; }


GH.tpl.gadget.unsupportedBrowser.message = function(opt_data, opt_ignored) {
  return '' + aui.message.warning({content: '<p>' + soy.$$escapeHtml('This gadget is not supported in your current browser.') + '</p><p><a href="' + soy.$$escapeHtml(opt_data.helpUrl) + '">' + soy.$$escapeHtml('More information') + '</a></p>'});
};
if (goog.DEBUG) {
  GH.tpl.gadget.unsupportedBrowser.message.soyTemplateName = 'GH.tpl.gadget.unsupportedBrowser.message';
}
;
;
/* module-key = 'com.pyxis.greenhopper.jira:gh-gadget-unsupported-browser', location = 'jira-agile-module/includes/js/gadgets/unsupported-browser.js' */
define("jira-agile/gadgets/unsupported-browser",["require"],function(e){"use strict";var r=e("jquery");return{showMessageIfUnsupported:function(e){return!!(r.browser.msie&&parseInt(r.browser.version,10)<=9)&&(e.getView().html(GH.tpl.gadget.unsupportedBrowser.message({helpUrl:{"url":"https://confluence.atlassian.com/display/JIRASOFTWARECLOUD/Supported+Platforms","isLocal":false}.url})),!0)}}});;