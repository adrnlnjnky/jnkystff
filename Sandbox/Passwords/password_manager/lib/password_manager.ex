defmodule PasswordManager.Options do
  def options() do
    choice = 
      IO.gets(
      "should we:
      1: get a password
      2: save a password
      3. change a password
      4. delete a password\n
      choice: "
    )
    |> String.trim("\n")
    |> run()
  end

  defp run("1"), do: PasswordManager.GetPassword.which_pass()
  defp run("2"), do: PasswordManager.EncodePassword.encode_pass()
  defp run("3"), do: "sorry not ready yet"
  defp run("4"), do: "sorry not ready yet"
  defp run(_), do: "Not a valid Selection"

end

PasswordManager.Options.options()

