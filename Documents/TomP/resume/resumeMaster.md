﻿---
title: A Resume for Tom Peltier II
dateCreated: 2020 9, 15
date: Thu Oct  7 09:50:15 AM PDT 2021
queary: yes
---

Tom Peltier
[Email:](adrnlnjnky@protonmail.com)

# Goal
  In a never ending quest to improve myself and learn new things, my goal is to become an excellent programmer.  I am
  seeking a job where I can grow as a programmer and a person.  While my primary job has not been in computers I have
  maintained networks for automotive companies, build computers from scratch for people and companies, written thousands
  of lines of code in the quest to learn new languages and solve problems with code.

# Coding

@. Linux
    *. The operating system I prefer.
@. Bash
    *. I'm actually using ZSH at the momment
    *. scripting simple tasks just saves time and brain space.
@. Go
    *. I really enjoy this simple and powerful language
@. Python
    *. Python is everywhere so I had to learn it.
@. CSS
    *. Gotta be able to style those web pages1
@. HTML
    *. Is it even an option not to know this language?
@. Elixir
    *. Functional programming is super cool
    *. This is my first dive into frameworks
        0. Learning Phoenix and LiveView

## Also (things I'm skilled with)

@. Windows
@. Spreadsheets (Excel)
@. Word processors



## Links
[Blog:](https://ecstatic-swirles-d8e8ec.netlify.app/)

[GitLab](https://www.gitlab.com/adrnlnjnky)

[GitHub](https://www.github.com/adrnlnjnky)



# Work History:


## River Guiding:


[Mountain and River Adventures](MTNRiver.com)

 *Operations Manager* March 2019 - Present

Accomplishments:

*. Successfully drove our client reviews to solid 5 stars.
*. Brought food costs down by 12%.
    #. This was accomplished through leveraging simple technolgy. ie spredsheets and python scripts.
*. Automated the payroll system

Duties:

*. Guide Recruiting
*. Guide Training (I think the hardest thing to teach is decistion making)
    #. Reading and navigating whitewater
    #. Swift water rescue
    #. Boatman skills
    #. Knot tying
    #. Guide Personability
    #. Client interaction
    #. Cooking
*. Work scheduling for
    #. Guides
    #. Drivers
    #. Food service personnel
    #. Support staff
*. Weekly management meetings
*. Create & Run fleet maintenance Program
    #. when I arrived fleet maintenance was fix it when it broke.  The company now has a functioning maintenance
    program.
*. Payroll Generation and Oversight
*. Automate the payroll system
    #. once again.. Everything was done by hand even though the software sweet offered time tacking and software.  I
    implimented the time tracking software for the company and worked with accounting to make it all happen smoothly.
*. Build in-house web application for Warehouse tasks and Guide information
   *. first in Go and then in Elixir
   $. this was a fun little project with chores, todos in checklists as well as an online copy of our schedule
*. Supervise Guide Post-trip (debrief) meetings
*. Conflict resolution
*. Update Guide Manual regularly
*. Any other responsibility that gets thrown at the side of my barn.


 **Lead River Guide** - Seasonal 2017 - 2018

*. Train guides
*. Team Leader
*. Run team meetings
    #. I successfully ran meeting to inspire and excite our staff to a five star reviews.
*. Lead by being a good example

[WhiteWaterVoyages](www.wwv.com)2015 - 2017

    Mentor River Guide
*. Train guides
*. Team Leader
*. Involved in meetings
*. Lead by being a good example

[Mountain and River Adventures](Mtnriver.com) 2009 - 2013

    Mentor River Guide
*. Train guides
*. Team Leader
*. Involved in meetings
*. Lead by being a good example

[Kern River Outfitters](Kern River Outfitters) 2007 - 2008

    Mentor River Guide

[Kern River Tours](Kern River Tours)  Seasons march 2005 - 2007

Senior River Guide 2005 - 2007


[United States Forrest Service](United States Forrest Service)

Summer season 1999
title: Forrest Ranger
    #. Maintain Forrest Service Presence on the river
    #. Interact with the public
    #. Interact with the permit holders
    #. Write the Policies and Procedures for the River Ranger Possition
        a. During my time here I co-authored the manual for standards and practices of a ranger for the Kern River.


[Chuck Richards Whitewater](Chuckrichards.com) Seasons from 1993 thru 1998
Guide to Senior Guide


## Off River Jobs


Ashe Electrical - Aug 2013 - spring 2015
    electrical work
    (Company moved to Tennesee)

Skinny Guy Plumbing - Aug 2008 - Spring 2013
    plumbing work
    (Owner has since retired and moved away)

Stone Contracting - Sept 2010 - Spring 2015
    finish carpentry
    tile
    water proofing

# Our Company
## Kern River Essentials

    Job Title: Co-owner

What KRO offer:

    *. Home Automation
       #. System design
       #. System instalation
            [ I typacaly set up video survailance systems that focus on personal security not just in the physical
            world.  Online and web based camera systems have security issues.








