---
title: FRAMASOFT 
author: tom peltier
date_modified: 2023-04-26 15:57:56
date_create: 2023-04-26 15:57:56
query: true
---

#  FRAMASOFT

* french non-profit
* privacy focused Open source
    * chatons --> find alternative services to google
    * framapad  --> replace google docs or notepad
        * stored online
        * saved for one year from last update
        * easy to share for colaboration.
    * framacalk   --> spreadsheet option.
        * basic spreadsheet
        * sharable
    * framaforms
    * framindmap
    * framimaps
    * calendar app
        * next cloud instance
    * framadate 
        * date poling for availability
    * Mobilizon
        * needs account
        * creates events 
    * Framatalk
        * video conferencing
        * Jitsy alternative
    * tlefriends
        * colaborative group stuff --> like facebook
    * framatine
        * slack clone
    * framlist  
        * mailing lists
    * peertube:
        * alternative to youtube
    * sepia search






* check out peer tube



