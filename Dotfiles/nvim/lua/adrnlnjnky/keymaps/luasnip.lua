-- ###############################################################################
-- ###############################################################################
-- ###                                                                         ###
-- ###                            NVIM CONFIGURATION:                          ###
-- ###                                  LuaSnip                                ###
-- ###                   Snippet plugin written in lua, very fast              ###
-- ###                                                                         ###
-- ###############################################################################
-- ###############################################################################

local ls = require("luasnip")
local types = require("luasnip.util.types")

-- local nmap = require("config.keymaps").nmap
-- local imap = require("config.keymaps").imap
-- local vmap = require("config.keymaps").vmap

vim.keymap.set({ "i", "s", "n" }, "<c-k>", function()
  if ls.expand_or_jumpable() then
    ls.expand_or_jump()
  end
end, { silent = true, desc = "LuaSnip: Expand or Jump" })

vim.keymap.set({ "i", "s", "n" }, "<c-j>", function()
  if ls.jumpable(-1) then
    ls.jump(-1)
  end
end, { silent = true, desc = "LuaSnip: Jump" })

vim.keymap.set("i", "<c-l>", function()
  if ls.choice_active() then
    ls.change_choice(1)
  end
end, { desc = "LuaSnip: Change Choice" })

vim.keymap.set(
  "n",
  "<leader><leader>s",
  "<cmd>source ~/.config/nvim/lua/adrnlnjnky/snips/init.lua<CR>",
  { desc = "Source Luasnips" }
)
