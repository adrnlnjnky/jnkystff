
Base4648](https://tools.ietf.org/html/rfc4648).│IyEvdXNyL2Jpbi9lbnYgcHl0aG9uMwoKaW1wb3J0IGNvZGVjcwppbXBvcnQgc3RyaW5nCmltcG9ydCBzeXMKaW1wb3J0IHRpbWUKCmZyb20gY3J5cHRvZ3JhcGh5Lmhhem1hdC5iYWNrZW5kcyBpbXBvcnQgZGVmYXVsdF9iYWNrZW5kCmZyb20gY3J5cHRvZ3JhcGh5Lmhhem1hdC5wcmltaXRpdmVzLmhhc2hlcyBpbXBvcnQgU0hBMQpmcm9tIGNyeXB0b2dyYXBoeS5oYXptYXQucHJpbWl0aXZlcy50d29mYWN0b3IudG90cCBpbXBvcnQgVE9UUAoKCk9ORV9XRUVLX0lOX1NFQ09ORFMgPSA2MDRfODAwCgoKZGVmIGdlbmVyYXRlX3NlY3JldCgpOgogICAgdG90cCA9IFRPVFAoCiAgICAgICAga2V5PWNvZGVjcy5lbmNvZGUoc3RyaW5nLmFzY2lpX2xldHRlcnMsIGVuY29kaW5nPSJ1dGYtOCIpLAogICAgICAgIGxlbmd0aD04LAogICAgICAgIGFsZ29yaXRobT1TSEExKCksCiAgICAgICAgdGltZV9zdGVwPU9ORV9XRUVLX0lOX1NFQ09ORFMsCiAgICAgICAgYmFja2VuZD1kZWZhdWx0X2JhY2tlbmQoKSwKICAgICkKICAgIHNlZWQgPSBpbnQodGltZS50aW1lKCkpCiAgICB0b2tlbiA9IGNvZGVjcy5kZWNvZGUodG90cC5nZW5lcmF0ZShzZWVkKSwgZW5jb2Rpbmc9InV0Zi04IikKICAgIHJldHVybiBmInt0b2tlbn0te3NlZWR9IgoKCiMgdGhlcmUgYXJlIDIgc21hbGwgYnVncyBiZWxvdyB5b3UnbGwgaGF2ZSB0byBmaXggYmVmb3JlIHByb2NlZWRpbmcgOikKaWYgX19uYW1lX18gPT0gIl9fbWFpbl9fIjoKICAgIHN5cy5zdGRvdXQoCiAgICAgICAgZiJQbGVhc2UgZ28gdG8gaHR0cHM6Ly9yYW1wLmNvbS9jYXJlZXJzIGFuZCB1c2UgdGhpcyBzZWNyZXQgd2hlbiAiCiAgICAgICAgZiJ5b3UgYXBwbHk6IHtnZW5lcmF0ZV9zZWNyZXR9XG4iCiAgICAp"


#!/usr/bin/env python3

import codecs
import string
import sys
import time

from cryptography.hazmat.backends import default_backend
from cryptography.hazmat.primitives.hashes import SHA1
from cryptography.hazmat.primitives.twofactor.totp import TOTP


ONE_WEEK_IN_SECONDS = 604_800


def generate_secret():
    totp = TOTP(
        key=codecs.encode(string.ascii_letters, encoding="utf-8"),
        length=8,
        algorithm=SHA1(),
        time_step=ONE_WEEK_IN_SECONDS,
        backend=default_backend(),
    )
    seed = int(time.time())
    token = codecs.decode(totp.generate(seed), encoding="utf-8")
    return f"{token}-{seed}"


# there are 2 small bugs below you'll have to fix before proceeding :)
if __name__ == "__main__":
    sys.stdout(
        f"Please go to https://ramp.com/careers and use this secret when "
        f"you apply: {generate_secret}\n"
    )
