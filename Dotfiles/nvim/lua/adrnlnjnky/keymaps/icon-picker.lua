-- ###############################################################################
-- ###############################################################################
-- ###                                                                         ###
-- ###                            NVIM CONFIGURATION:                          ###
-- ###                               ICON-PICKER:                              ###
-- ###       Pick ALT Font Characters ,Symbols, nerd Font Icons & Emojis       ###
-- ###                     🥲   🍅  🧭  𝛔 𝙹 🧲   ¶  ⨈   🧰                     ###
-- ###                                                                         ###
-- ###############################################################################
-- ###############################################################################

local nmap = require("config.keymaps").nmap
local imap = require("config.keymaps").imap
local wk = require("which-key")

wk.register({
  ["<leader>i"] = {
    name = "Icon Picker",
    ["n"] = {
      name = "+Normal",
      a = { "<CMD>IconPickerNormal<CR>", "All" },
      e = { "<CMD>IconPickerNormal emoji<CR>", "Emojis" },
      f = { "<CMD>IconPickerNormal alt_font<CR>", "Fonts" },
      n = { "<CMD>IconPickerNormal nerd_font<CR>", "Nerd Font" },
      s = { "<CMD>IconPickerNormal symbols<CR>", "Symbols" },
    },
    ["y"] = {
      name = "+Yank",
      a = { "<CMDIconPickerYank<CR>", "All" },
      e = { "<CMDIconPickerYank emoji<CR>", "Emojis" },
      f = { "<CMDIconPickerYank alt_font<CR>", "Fonts" },
      n = { "<CMDIconPickerYank<CR> nerd_font", "Nerd Font" },
      s = { "<CMDIconPickerYank symbols<CR>", "Symbols" },
    },
  },
})

-- imap({ "<c-ia>", "<cmd>IconPickerInsert<cr>", desc = "Icon Picker Insert All" })
-- imap({ "<c-is>", "<cmd>IconPickerInsert alt_font symbols<cr>", desc = "Icon Picker Insert: Symbols" })
-- imap({ "<c-ie>", "<cmd>IconPickerInsert nerd_font emoji<cr>", desc = "Icon Picker Insert: Emojis" })
-- imap({ "<c-i>", "<cmd>IconPickerInsert emoji<cr>", desc = "Icon Picker Insert: Emojis" })
