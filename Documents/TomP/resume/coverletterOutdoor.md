As a team leader and manager of a class V rafting company I am well versed in dealing with mission critical situations.
I take ownership of situations as they arise, in the wilderness there is no time to small things to grow into big
things.  Every situation must be handled, and handled well, the success of the trip depends on it.

I spent many years working the wilderness taking folks out and keeping them safe, and managing the teams that do that. Empathy and companion are at the center of quality guiding, skills I look forward to bringing to my coding career.

I didn't talk about it in my resume but I feel like I should mention here that my first language was basic.  I wrote on and Apple IIe and I have dabbled with computers off and on ever since. In the 90's I took time from my college degree path to take classes in things like C and Pascal.  In the early 2000's I maintained a Windows95 server based network for an Automotive company that I was working for. The last two years I've been working to bring my skills back around, first in Python, then Go and now in Elixir.

I enjoy a challenge and constant learning is an important aspect of my life.  In the Wilderness constant evaluation of the situation around my trip and prioritizing based on that evaluation is just a part of the job.  I look forward to bringing my problem solving skills and dynamic approach to life into my next endeavor.

One of my greatest frustrations as a manager has been in the software.  The system I've been using the last several
years is both slow with many expensive steps involved in my day to day tasks and not very efficient for my companies
needs.  While I love to code, when sitting in front of a computer takes away from a productive day it is not good for
the employee or the employer.  Improving software and saving workers time makes for a happier employee and a stronger
company.  I want to be a part of the answer, I want to write strong code that makes peoples lives better.

Thanks for taking the time to read over my resume and cover letter and I hope to hear from you soon. Remember to smile it might hurt!!


Tom Peltier
661 619-5677
adrnlnjnky@protonmail.com
adrnlnjnky@gmail.com
gitlab.com/adrnlnjnky


