-- ###############################################################################
-- ###############################################################################
-- ###                                                                         ###
-- ###                               CONFIGURATION:                            ###
-- ###                                   NVIM:                                 ###
-- ###            Entry point for Nvim configuration files init.lua            ###
-- ###                                                                         ###
-- ###############################################################################
-- ###############################################################################

vim.g.mapleader = " "
vim.g.maplocalleader = ","

-- linking is cool.
--
--
require("adrnlnjnky.globals")

require("config.lazy")

require("adrnlnjnky.keymaps")

-- require("elixirls").setup()
-- require("elixir").setup()

require("notify").setup({
  background_colour = "#000000",
})
