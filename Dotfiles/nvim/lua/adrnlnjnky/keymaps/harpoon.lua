-- ###############################################################################
-- ###############################################################################
-- ###                                                                         ###
-- ###                            NVIM CONFIGURATION:                          ###
-- ###                                  HARPOON                                ###
-- ###                   Quick switching between a few buffers                 ###
-- ###                                                                         ###
-- ###############################################################################
-- ###############################################################################

local wk = require("which-key")
local nmap = require("config.keymaps").nmap
-- local imap = require("config.keymaps").imap
-- local vmap = require("config.keymaps").vmap

wk.register({
  [","] = {
    name = "Harpoon",
    a = { require("harpoon.mark").add_file, "Add File" },
    m = { require("harpoon.ui").toggle_quick_menu, "Toggle Quick Menu" },

    j = { require("harpoon.ui").nav_next(), "goto Harpoon 1" },
    k = { require("harpoon.ui").nav_prev(), "goto Harpoon 1" },
    f = { require("harpoon.ui").nav_file(1), "goto Harpoon 1" },
    d = { require("harpoon.ui").nav_file(2), "goto Harpoon 2" },
    -- f = { require("harpoon.ui").nav_file(3) },
    -- d = { require("harpoon.ui").nav_file(4) },
  },
})

-- wk.register({
-- ["<leader>"] = {
-- j = { require("harpoon.ui").nav_next(), "Harpoon Next" },
-- k = { require("harpoon.ui").nav_prev(), "Harpoon Previous" },
-- },
-- })

-- vim.keymap.set({ "n", "<leader>j", require("harpoon.ui").nav_file(1), { desc = "Navigate to Harpoon Mark 1" } })
-- vim.keymap.set({ "n", "<leader>2", require("harpoon.ui").nav_file(2), { desc = "Navigate to Harpoon Mark 2" } })
-- vim.keymap.set({ "n", "<leader>3", require("harpoon.ui").nav_file(3), { desc = "Navigate to Harpoon Mark 3" } })
-- vim.keymap.set({ "n", "<leader>4", require("harpoon.ui").nav_file(4), { desc = "Navigate to Harpoon Mark 4" } })
-- vim.keymap.set({ "n", "<leader>5", require("harpoon.ui").nav_file(5), { desc = "Navigate to Harpoon Mark 5" } })
