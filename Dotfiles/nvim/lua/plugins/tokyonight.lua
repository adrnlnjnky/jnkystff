-- ###############################################################################
-- ###############################################################################
-- ###                                                                         ###
-- ###                            NVIM CONFIGURATION:                          ###
-- ###                                TOKYONIGHTS                              ###
-- ###                Colorscheme that celbrates Tokyo at night               ###
-- ###                                                                         ###
-- ###############################################################################
-- ###############################################################################

-- local opts = { mode = "n", noremap = false, nowait = true, silent = true }

return {
  {
    "folke/tokyonight.nvim",
    -- lazy = true,
    priority = 1000,
    opts = {
      transparent = true,
      styles = {
        sidebars = "transparent",
        floats = "transparent",
      },
      config = function()
        vim.cmd([[colorscheme tokyonight-storm ]])
      end,
    },
    keys = {
      { "<leader>wcd", ":colorscheme tokyonight<CR>", desc = "Tokyonight Day" },
      { "<leader>wcm", ":colorscheme tokyonight<CR>", desc = "Tokyonight Moon" },
      { "<leader>wcn", ":colorscheme tokyonight<CR>", desc = "Tokyonight Night" },
      { "<leader>wcs", ":colorscheme tokyonight<CR>", desc = "Tokyonight Storm" },
      { "<leader>wct", ":coloracheme tokyonight<CR>", desc = "Tokyonight" },
    },
  },
}
