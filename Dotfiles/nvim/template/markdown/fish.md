#!usr/bin/env fish
set -e

:'
README: 
    -h --> Help flag
:'

set -l options (fish_opt -s s -l help)
## using fish_opt makes it easy to set several flags.  help is the first flag set.
# set options $options (fish_opt -s <short_letter> -l <long_name>)

argparse $options -- $argv

if set -q _flag_help
    echo ""
    echo "NEW SESSION HELP:"
    echo "-----------------"
    echo "-h = HELP"
    echo ""
    exit 0
end

{{_cursor_}}
