-- ###############################################################################
-- ###############################################################################
-- ###                                                                         ###
-- ###                            NVIM CONFIGURATION:                          ###
-- ###                                 BISCUITS                                ###
-- ###             Because every developer needs some buscuit love             ###
-- ###                                                                         ###
-- ###############################################################################
-- ###############################################################################

return {
  {
    "code-biscuits/nvim-biscuits",

    opts = {
      default_config = {
        max_length = 12,
        min_distance = 11,
        prefix_string = " 📎 ",
        toggle_keybind = "<leader>cb",
      },
      language_config = {
        elixir = {
          prefix_string = "🙀",
          min_distance = 20,
        },
        html = {
          prefix_string = " 🌐 ",
        },
        javascript = {
          prefix_string = "  ",
          max_length = 80,
        },
        lua = {
          prefix_string = "  ",
          max_length = 80,
        },
        markdown = {
          disabled = true,
        },
        -- python = {
        --   disabled = true,
        -- },
      },
    },
  },
}
