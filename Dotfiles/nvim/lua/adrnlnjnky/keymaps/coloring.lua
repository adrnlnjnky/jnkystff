-- ###############################################################################
-- ###############################################################################
-- ###                                                                         ###
-- ###                            NVIM CONFIGURATION:                          ###
-- ###                                 KEYPINGS                                ###
-- ###                Keyboard shortcuts for all sorts of commands             ###
-- ###                                                                         ###
-- ###############################################################################
-- ###############################################################################

local nmap = require("config.keymaps").nmap
local wk = require("which-key")

wk.register({
  ["<leader>cs"] = {
    c = { ":colorscheme catppuccin<CR>", "catppuccin" },
  },
})

wk.register({
  ["<leader>cs"] = {
    name = "ColorScheme",
    o = { "<CMD>colorscheme oceanic-material<CR>", "Oceanic Material" },
  },
})
-- Color My Pencils
nmap({ "<leader>csp", "<cmd>colorscheme ColorMyPencils()<CR>", { desc = "Color My Pencils" } })

-- Oceaniic-Materials
-- nmap({ "<leader>cso", "<cmd>colorscheme oceanic-material<CR>", { desc = "Oceanic Material" } })

-- Rose-Pine
wk.register({
  ["<leader>csr"] = {
    name = "+Rose Pine",
    a = { "<CMD>colorscheme rose-pine<CR>", "Rose-Pine Main" },
    m = { "<CMD>colorscheme rose-pine moon<CR>", "Rose-Pine Moon" },
    d = { "<CMD>colorscheme rose-pine dawn<CR>", "Rose-Pine Dawn" },
  },
})
-- Kanagawa
wk.register({
  ["<leader>csk"] = {
    name = "+Kanagawa",
    w = { "<CMD>colorscheme kanagawa-wave<CR>", "Kanagawa Wave" },
    d = { "<CMD>colorscheme kanagawa-dragon<CR>", "Kanagawa Dragon" },
    l = { "<CMD>colorscheme kanagawa-lotus<CR>", "Kanagawa Lotus" },
  },
})
