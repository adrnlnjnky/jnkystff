-- %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
-- %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
-- %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
-- %%%%%%  %%%%  %%%%%%%   %%%%%  %%%%          %%%%%%
-- %%%%%%  %%%%  %%%%%%    %%%%%  %%%%          %%%%%%
-- %%%%%%  %%%%  %%%%  %%  %%%%%  %%%%%%%%  %%%%%%%%%%
-- %%%%%%  %%%%  %%%  %%%  %%%%%  %%%%%%%%  %%%%%%%%%%
-- %%%%%%  %%%%  %%  %%%%  %%%%%  %%%%%%%%  %%%%%%%%%%
-- %%%%%%  %%%%  %  %%%%%  %%%%%  %%%%%%%%  %%%%%%%%%%
-- %%%%%%  %%%%    %%%%%%  %%%%%  %%%%%%%%  %%%%%%%%%%
-- %%%%%%  %%%%  %%%%%%%%  %%%%%  %%%%%%%%  %%%%%%%%%%
-- %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
-- %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
-- %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

require('adrnlnjnky.settings')
require('adrnlnjnky.packer_config')
require('adrnlnjnky.keymaps')

require('adrnlnjnky.tele')

require('adrnlnjnky.completion')


require('adrnlnjnky.lspsaga')
require('adrnlnjnky.lsp')
require('adrnlnjnky.dap')


require('adrnlnjnky.snips')
require('adrnlnjnky.snips.all')
require('adrnlnjnky.snips.ft.elixir')
require('adrnlnjnky.snips.ft.lua')
-- require('adrnlnjnky.snips.ft.go')

require('adrnlnjnky.biscuits')

require'luasnip'.filetype_extend("html", {"html"})


-- Lightbulb stuff
vim.cmd [[autocmd CursorHold,CursorHoldI * lua require'nvim-lightbulb'.update_lightbulb()]]
-- vim.api.nvim_command('highlight LightBulbFloatWin ctermfg=#A324B2 ctermbg=#A324B2 guifg=#A324B2 guibg=#A324B2')
-- vim.api.nvim_command('highlight LightBulbVirtualText ctermfg=#A324B2 ctermbg=#A324B2 guifg=#A324B2 guibg=#A324B2')



