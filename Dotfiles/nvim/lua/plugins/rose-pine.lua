-- ###############################################################################
-- ###############################################################################
-- ###                                                                         ###
-- ###                            NVIM CONFIGURATION:                          ###
-- ###                                  TELESCOPE                              ###
-- ###                  Highly extendable fuzzy finder over lists.             ###
-- ###                                                                         ###
-- ###############################################################################
-- ###############################################################################

return {
  "rose-pine/neovim",
  name = "rose-pine",
  opts = {
    dark_variant = "main",
    bold_vert_split = true,
    dim_nc_background = true,
    disable_background = true,
    disable_float_background = true,
    disable_italics = false,

    groups = {
      background = "base",
      backgrounnd_nc = "_experimental_nc",
      panel = "surface",
      panel_nc = "base",
      border = "highlight_med",
      comment = "muted",
      link = "iris",
      punctuation = "subtle",

      error = "love",
      hint = "iris",
      info = "foam",
      warn = "gold",

      headings = {
        h1 = "iris",
        h2 = "foam",
        h3 = "rose",
        h4 = "gold",
        h5 = "pine",
        h6 = "foam",
      },
    },
    highlight_groups = {
      ColorColumn = { bg = "rose" },
      CursorLine = { bg = "foam", blend = 10 },
      StatusLine = { fg = "love", blend = 10 },
    },
  },
}
