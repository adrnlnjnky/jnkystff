import XMonad

import XMonad.Hooks.DynamicLog
import XMonad.Hooks.ManageDocks

import XMonad.Util.EZConfig
import XMonad.Util.Loggers
import XMonad.Util.Ungrab
import XMonad.Util.SpawnOnce

import XMonad.Hooks.EwmhDesktops

main :: IO ()
main = xmonad 
     . ewmhFullscreen
     . ewmh
     $ myConfig

myconfig = def
    {
      modMask    = mod4Mask
    , layoutHook = myLayout
    , startupHook  = myStartupHook
    }
    `additionalKeysP`
      [ ("M-S-z",   spawn   "xscreensaver-command -lock")
      , ("M-C-s",   unGrab  *> spawn "scrot -s"         )
      , ("M-v",     spawn   "vivaldi"                   )
      , ("M-b",     spawn   "brave"                     )
      , ("M-a",     spawn   myTerminal                  )
      -- , ("M-a",     spawn   "alacritty"                 )
      , ("M-g",     spawn   "gimp"                      )
      ]
      
myManageHook :: ManageHook 
myManageHook = composeAll
  [ className =? "Gimp"   --> doFloat
  , isDialog              --> doFloat
  ]
    
myLayout = tiled ||| Mirror tiled ||| Full ||| threeCol
    where 
      threeCol  = magnifiercz' 1.3 $ ThreeColMid nmaster delta ratio
      tiled     = Tall nmaster delta ratio
      nmaster   = 1     -- number of windows in the master pane
      ratio     = 1/2   -- proportion of screen occupied by master pane
      delta     = 3/100 -- Percent of screen to increment by when resizing panes

MyStartupHook :: Startup 
MyStartupHook = do
[ (spawnOnce "vivaldi")
, (spawnOnce "alacritty")
, (spawnOnce "brave")
]

MyWorkspaces :: [String]
myWorkspaces = ["main", "video", "Comms", "Dev"]

myTerminal :: IsString a => a
myTerminal = "alacritty"


