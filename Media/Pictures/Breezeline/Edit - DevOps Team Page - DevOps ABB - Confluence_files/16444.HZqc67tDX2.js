"use strict";(self._cf=self._cf||[]).push([[16444],{597819:(e,t,n)=>{n.d(t,{i:()=>d});var o=n(991332),r=n(29298),i=n(122551),a=n(42198),l=n(917068);const s=async()=>{};function d(e){class t extends i.Component{render(){return i.createElement(l.e,null,(({cloudId:t})=>{const n="true"===sessionStorage.getItem("confluence.disable-engagement-for-tests")?{start:s,stop:s}:new a.Z(t,"/gateway/api");return i.createElement(e,(0,o.Z)((0,o.Z)({},this.props),{},{coordinationClient:n}))}))}}return t.displayName="WithCoordinationClient",(0,r.Z)(t,"displayName",`withCoordinationClient(${e.displayName||e.name})`),t}},710246:(e,t,n)=>{n.d(t,{T:()=>m});var o=n(122551),r=n(538639),i=n(772610),a=n(34888),l=n(464984),s=n(524798),d=n(970045),c=n(692015),u=n(325834),p=n(597819);const f=(0,l.defineMessages)({title:{id:"growth-experiment-helpers.better-together.nudge.title",defaultMessage:"Get back to Jira Software"},description:{id:"growth-experiment-helpers.better-together.nudge.description",defaultMessage:"Use this shortcut to quickly get back to your connected Jira project."},button:{id:"growth-experiment-helpers.better-together.nudge.acknowledge-button",defaultMessage:"Got it"}}),h=({coordinationClient:e,children:t})=>{const{formatMessage:n}=(0,s.Z)(),[r,i]=(0,a.Y)(e,"confluence-project-pages-cross-join"),[l,p]=(0,o.useState)(!1),{isLeftSidebarCollapsed:h}=(0,c.Z_)(),{createAnalyticsEvent:g}=(0,u._)();(0,o.useEffect)((()=>{r&&p(!0)}),[r]),(0,o.useEffect)((()=>()=>{l&&i()}),[l,i]);const m=(0,o.useCallback)((e=>{g({type:"sendTrackEvent",data:{action:e,source:"spaceShortcuts",actionSubject:"nudgeSpotlight",actionSubjectId:"jiraProjectLinkSpotlight"}}).fire()}),[g]),v=(0,o.useCallback)((()=>m("shown")),[m]),b=(0,o.useCallback)((()=>{p(!1),m("hidden")}),[p,m]),y=(0,o.useCallback)((()=>{m("clicked"),b()}),[b,m]);return o.createElement(d.W,{width:275,heading:n(f.title),content:n(f.description),position:"right-start",autoShow:!0,offset:[0,16],hideSpotlightOnOutsideClick:!0,concealSpotlightOnReferenceHidden:!0,hidden:h||!l,onOutsideClick:b,onShow:v,actions:[{text:n(f.button),onClick:y}],setHidden:b},t)};h.displayName="JiraProjectLinkSpotlightComponent";const g=(0,p.i)(h),m=({children:e,selector:t})=>{const{location:n}=(0,o.useContext)(i.M),a=Boolean(r.y4.match((null==n?void 0:n.pathname)||""));return t&&/external_link jira-project/.test(t)&&a?o.createElement(g,null,e):e}},524385:(e,t,n)=>{n.d(t,{J:()=>o});const o=(0,n(28201).ZR)({__loadable_id__:"Rsuii",name:"SpaceShortcutsDialog",loader:async()=>(await Promise.all([n.e(12705),n.e(61769),n.e(78248),n.e(46003),n.e(11956),n.e(55543),n.e(17414),n.e(64862)]).then(n.bind(n,885193))).SpaceShortcutsDialog})},174078:(e,t,n)=>{n.d(t,{r:()=>l});var o=n(122551),r=n(883383),i=n(370243),a=n(24501);const l=({onClick:e})=>o.createElement(i.Z,{key:"addShortcutLink",iconBefore:o.createElement(a.default,{label:""}),onClick:e},o.createElement(r.Z,{id:"space-shortcuts.shortcut.add",defaultMessage:"Add shortcut"}));l.displayName="AddShortcutButton"},24501:(e,t,n)=>{Object.defineProperty(t,"__esModule",{value:!0}),t.default=void 0;var o,r=(o=n(122551))&&o.__esModule?o:{default:o},i=n(386056);const a=e=>r.default.createElement(i.Icon,Object.assign({dangerouslySetGlyph:'<svg width="24" height="24" viewBox="0 0 24 24" role="presentation"><path d="M13 11V3.993A.997.997 0 0012 3c-.556 0-1 .445-1 .993V11H3.993A.997.997 0 003 12c0 .557.445 1 .993 1H11v7.007c0 .548.448.993 1 .993.556 0 1-.445 1-.993V13h7.007A.997.997 0 0021 12c0-.556-.445-1-.993-1H13z" fill="currentColor" fill-rule="evenodd"/></svg>'},e));a.displayName="AddIcon";var l=a;t.default=l},877575:(e,t,n)=>{Object.defineProperty(t,"__esModule",{value:!0}),t.default=void 0;var o,r=(o=n(122551))&&o.__esModule?o:{default:o},i=n(386056);const a=e=>r.default.createElement(i.Icon,Object.assign({dangerouslySetGlyph:'<svg width="24" height="24" viewBox="0 0 24 24" role="presentation"><path d="M4.02 19.23a1 1 0 001.18 1.18l3.81-.78-4.21-4.21-.78 3.81zM19.844 6.707l-2.12-2.122A1.997 1.997 0 0016.308 4c-.512 0-1.024.195-1.415.585l-9.757 9.758 4.95 4.95 9.757-9.758a2 2 0 000-2.828z" fill="currentColor" fill-rule="evenodd"/></svg>'},e));a.displayName="EditFilledIcon";var l=a;t.default=l},966186:(e,t,n)=>{n.d(t,{Z:()=>i});var o=n(122551),r=n(396598);const i=e=>o.createElement(r.Z,{appear:!0},e.children)},628738:(e,t,n)=>{n.d(t,{Z:()=>s});var o=n(953307),r=n(122551),i=n(800544),a=n(568538),l=n(181844);const s=e=>{const{shouldRender:t}=(0,l.mO)();if(!t)return null;const n=(0,a.UL)(a.NE,e.cssFn);return r.createElement(i.Z,(0,o.Z)({},e,{cssFn:n}))}},42198:(e,t,n)=>{n.d(t,{Z:()=>r});var o=n(999407);class r{constructor(e,t){if(!e||!t)throw new Error("Need a valid CloudId and Stargate url");this.cloudId=e,this.stargateUrl=t}async start(e,t){return(0,o.jQ)(this.cloudId,this.stargateUrl,e,t)}async stop(e){return(0,o.Jk)(this.cloudId,this.stargateUrl,e)}}},34888:(e,t,n)=>{n.d(t,{Y:()=>r});var o=n(122551);const r=(e,t)=>{const[n,r]=(0,o.useState)(!1);(0,o.useEffect)((()=>{(async()=>{try{await e.start(t)&&r(!0)}catch(n){console.error(`Unable to start flow message ${t}. ${n}`)}})()}),[e,t]);return[n,async o=>{if(o||n)try{await e.stop(t)}catch(r){console.error(`Unable to stop flow message ${t}. ${r}`)}}]}},970045:(e,t,n)=>{n.d(t,{W:()=>R});var o=n(991332),r=n(415911),i=n(953307),a=n(122551),l=n(438936),s=n(131436),d=n(676424);const c=s.kJ,u=l.F4`
   0% {
    opacity: 0;
  }

  50% {
    opacity: 1;
  }

  100% {
    opacity: 0;
  }
`,p=l.iv`
  animation: ${u} 3000ms ${"cubic-bezier(0.55, 0.055, 0.675, 0.19)"} infinite;
`,f=l.iv`
  content: '';
  position: absolute;
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;
`,h=l.ZP.span`
  display: block;
  > * {
    position: relative;

    &::before {
      ${f}
      border-radius: ${({pulseBorderRadius:e=d.E0})=>e}px;
      box-shadow: ${({pulseColor:e=c})=>`0 0 0 3px ${e}`};
      opacity: ${e=>e.hasPulse?1:0};
      transition: opacity 0.1s ease-in-out;
      pointer-events: none;
    }

    &::after {
      ${f}
      border-radius: ${({pulseBorderRadius:e=d.E0})=>e}px;
      box-shadow: ${({pulseColor:e=c,pulseShadowColor:t="rgba(101, 84, 192, 1)"})=>`0 0 0 3px ${e} , 0 0 11px ${t}`};
      opacity: 0;
      pointer-events: none;
      ${e=>e.hasPulse&&p}
    }
  }
`,g=(0,a.forwardRef)(((e,t)=>{let{children:n,nudgeTestId:o}=e,l=(0,r.Z)(e,["children","nudgeTestId"]);return a.createElement(h,(0,i.Z)({},l,{innerRef:t,"data-testid":o}),n)}));var m=n(52054),v=n(485855),b=n(47656);const y={color:s.N0,boxShadow:`0 4px 8px -2px ${s.Nx}, 0 0 1px ${s.VG}`,backgroundColor:s.Wn,borderRadius:"3px",overflow:"auto"},w={display:"flex",flexDirection:"column",padding:"16px 20px"},C=l.ZP.h4`
  font-size: 1.42857em;
  font-style: inherit;
  line-height: 1.2;
  font-weight: 500;
  letter-spacing: -0.008em;
  margin-top: 28;
  color: inherit;

  &:first-child {
    margin-top: 0;
  }
`,x={display:"flex",paddingBottom:"8px",alignItems:"baseline",justifyContent:"space-between"},E={display:"flex",paddingTop:"8px",alignItems:"center",justifyContent:"space-between"},k=l.ZP.button`
  margin: 0 ${4}px;
  cursor: pointer;
  align-items: baseline;
  border-width: 0px;
  border-radius: 3px;
  box-sizing: border-box;
  display: inline-flex;
  font-size: inherit;
  font-style: normal;
  font-family: inherit;
  font-weight: 500;
  max-width: 100%;
  position: relative;
  text-align: center;
  text-decoration: none;
  transition: background 0.1s ease-out 0s,
    box-shadow 0.15s cubic-bezier(0.47, 0.03, 0.49, 1.38) 0s;
  white-space: nowrap;
  background: ${({index:e})=>e?s.Wn:s.HD};
  cursor: pointer;
  height: 2.28571em;
  line-height: 2.28571em;
  padding: 0px 10px;
  vertical-align: middle;
  width: auto;
  justify-content: center;
  color: ${s.N0};
  &:hover {
    background-color: ${s.vJ};
  }
`,S={display:"flex",margin:"-4px",flexDirection:"row-reverse"},$=(0,a.forwardRef)((({image:e,heading:t,actions:n,actionsBeforeElement:r,width:i=300,cardTestId:l,content:s},d)=>{const c=(0,a.useMemo)((()=>null==n?void 0:n.map((({text:e,onClick:t},n)=>a.createElement(k,{index:n,onClick:t,key:`${e}-${n}`},e)))),[n]),u=!!t,p=!!r||!!n;return a.createElement("div",{ref:d,style:(0,o.Z)((0,o.Z)({},y),{},{width:`${i}px`}),"data-testid":l},"string"==typeof e?a.createElement("img",{src:e,alt:""}):e,a.createElement("div",{style:w},u&&a.createElement("div",{style:x},a.createElement(C,null,t)),s,p&&a.createElement("div",{style:E},r||a.createElement("span",null),a.createElement("div",{style:S},c))))})),Z=l.F4`
  0% {
    opacity: 0;
  }
  100% {
    opacity: 1;
  }
`,I=l.ZP.div`
  ${({animateIn:e})=>e&&`animation: 0.2s ${Z} ease-out;`}
  animation-iteration-count: 1;
`,O=({fn:e,relay:t})=>((0,a.useEffect)((()=>{t((()=>e))}),[e,t]),null),R=e=>{let{hidden:t,setHidden:n,hideNudgeOnClick:l=!0,hideSpotlightOnOutsideClick:s=!0,concealSpotlightOnReferenceHidden:d=!1,autoShow:c=!0,children:u,spotlight:p=$,nudge:f=g,position:h="bottom",offset:y,zIndex:w=800,onRender:C,onShow:x,onHide:E,onClick:k,onOutsideClick:S,pulseColor:Z,pulseShadowColor:R,pulseBorderRadius:_,registerUpdateFn:j,registerToggleCardFn:M,nudgeBlinkId:P,nudgeTestId:H}=e,N=(0,r.Z)(e,["hidden","setHidden","hideNudgeOnClick","hideSpotlightOnOutsideClick","concealSpotlightOnReferenceHidden","autoShow","children","spotlight","nudge","position","offset","zIndex","onRender","onShow","onHide","onClick","onOutsideClick","pulseColor","pulseShadowColor","pulseBorderRadius","registerUpdateFn","registerToggleCardFn","nudgeBlinkId","nudgeTestId"]);const T=(0,a.useRef)();T.current={onRender:C,onHide:E,onShow:x,onClick:k,onOutsideClick:S};const B=(0,a.useRef)(null),A=(0,a.useRef)(null),z=(0,a.useMemo)((()=>p===$?(0,o.Z)({},N):{cardTestId:N.cardTestId}),[p,N]),F=!t,[U,J]=(0,a.useState)(c),[L,D]=(0,a.useState)(!1),[G,W]=(0,a.useState)(!0),V=(0,a.useCallback)((()=>W(!1)),[]),q=(0,a.useCallback)((()=>F&&J(!0)),[F]),Y=(0,a.useCallback)((e=>{var o,r;t||(l&&n(e),null===(o=T.current)||void 0===o||null===(r=o.onClick)||void 0===r||r.call(o))}),[t,l,n,T]),[Q,K]=(0,a.useState)();return(0,a.useEffect)((()=>{Q&&(null==j||j((()=>Q)))}),[Q,j]),(0,a.useEffect)((()=>{J&&(null==M||M((()=>J)))}),[J,M]),(0,a.useEffect)((()=>{D(F&&U)}),[F,U]),(0,a.useEffect)((()=>{var e,n;if(!t)return null===(e=T.current)||void 0===e||null===(n=e.onRender)||void 0===n||n.call(e),()=>{var e,t;return null===(e=T.current)||void 0===e||null===(t=e.onHide)||void 0===t?void 0:t.call(e)}}),[t,T]),(0,a.useEffect)((()=>{var e,t;L&&(null===(e=T.current)||void 0===e||null===(t=e.onShow)||void 0===t||t.call(e))}),[L,T]),(0,a.useEffect)((()=>{t&&J(c)}),[t,c]),(0,a.useEffect)((()=>{let e,t;return L&&s&&(t=setTimeout((()=>{e=(0,b.ak)(window,{type:"click",listener:t=>{const n=t.target;if(B.current&&A.current&&n instanceof Node&&!B.current.contains(n)&&!A.current.contains(n)){var o,r,i;if(P){if(((e,t,n)=>{const o=Array.from(document.querySelectorAll(`[data-nudge-blink='${e}']`));return!!(null==o?void 0:o.some((e=>e&&e.contains(t))))&&(n(!0),!0)})(P,n,W))return}J(!1),null===(o=T.current)||void 0===o||null===(r=o.onOutsideClick)||void 0===r||r.call(o),null===(i=e)||void 0===i||i()}},options:{capture:!0,passive:!0}})}))),()=>{var n;null===(n=e)||void 0===n||n(),clearTimeout(t)}}),[L,s,n,T,P]),a.createElement(a.Fragment,null,a.createElement(f,{ref:B,hasPulse:F,onClickCapture:Y,onMouseEnter:q,pulseColor:Z,pulseShadowColor:R,pulseBorderRadius:_,nudgeTestId:H},u),L&&a.createElement(m.Z,{zIndex:w},a.createElement(v.r,{placement:h,referenceElement:B.current||void 0,offset:y},(({ref:e,style:t,forceUpdate:n,isReferenceHidden:r})=>a.createElement(I,{onAnimationEnd:V,animateIn:G,innerRef:e,style:(0,o.Z)((0,o.Z)({},t),d&&r?{display:"none"}:{})},a.createElement(O,{fn:n,relay:K}),a.createElement(p,(0,i.Z)({},z,{ref:A})))))))}},613433:function(e,t,n){var o=this&&this.__assign||function(){return(o=Object.assign||function(e){for(var t,n=1,o=arguments.length;n<o;n++)for(var r in t=arguments[n])Object.prototype.hasOwnProperty.call(t,r)&&(e[r]=t[r]);return e}).apply(this,arguments)};Object.defineProperty(t,"__esModule",{value:!0}),t.bindAll=void 0;var r=n(213350);function i(e){if(void 0!==e)return"boolean"==typeof e?{capture:e}:e}t.bindAll=function(e,t,n){var a=t.map((function(t){var a=function(e,t){return null==t?e:o(o({},e),{options:o(o({},i(t)),i(e.options))})}(t,n);return r.bind(e,a)}));return function(){a.forEach((function(e){return e()}))}}},213350:(e,t)=>{Object.defineProperty(t,"__esModule",{value:!0}),t.bind=void 0,t.bind=function(e,t){var n=t.type,o=t.listener,r=t.options;return e.addEventListener(n,o,r),function(){e.removeEventListener(n,o,r)}}},47656:(e,t,n)=>{var o=n(213350);Object.defineProperty(t,"ak",{enumerable:!0,get:function(){return o.bind}});var r=n(613433)}}]);
//# sourceMappingURL=16444.HZqc67tDX2.js.map