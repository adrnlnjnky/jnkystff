local ls = require("luasnip")
-- some shorthands...
local s = ls.snippet
local sn = ls.snippet_node
local t = ls.text_node
local i = ls.insert_node
local f = ls.function_node
local c = ls.choice_node
local d = ls.dynamic_node
local r = ls.restore_node
local l = require("luasnip.extras").lambda
local rep = require("luasnip.extras").rep
local p = require("luasnip.extras").partial
local m = require("luasnip.extras").match
local n = require("luasnip.extras").nonempty
local dl = require("luasnip.extras").dynamic_lambda
local fmt = require("luasnip.extras.fmt").fmt
local fmta = require("luasnip.extras.fmt").fmta
local types = require("luasnip.util.types")
local conds = require("luasnip.extras.expand_conditions")

local snippets, autosnippets = {}, {}

local something = " use Ecto "

local get_test_result = function(position)
  --Todo
  return d(position, function()
    local nodes = {}
    table.insert(nodes, t " ")

    local lines = vim.api.nvim_buf_set_lines(0, 0, -1, false)
    for _, line in ipairs(lines) do
      if line:match("use Ecto") then
        table.insert(nodes, t " --> Results ")
        break
      end
    end




    return sn(nil, t "example")
  end, {})
end



ls.add_snippets("lua", {

    s(
    "dynam",
    fmt(
    [[
    {} and then {} or

    maybe {}

    ]],
    {
      i(1, "hold"),
      -- i(2),
      get_test_result(2),
      i(0),
    }
    )
    ),


  s("reqq",
  fmt([[local {} = require "{}"]],
  {
    f(function(import_name)
        local parts = vim.split(import_name[1][1], ".", true)
        return parts[#parts] or ""
      end, { 1 }),
    i(1)})
    )


})




-- local ls = require "luasnip"

-- local snippet = ls.s
-- local snippet_from_nodes = ls.sn

-- local i = ls.insert_node
-- local t = ls.text_node
-- local d = ls.dynamic_node
-- local c = ls.choice_node
-- local f = ls.function_node
-- local fmt = require("luasnip.extras.fmt").fmt

-- -- local shared = R "adrnljnky.snips"
-- -- local same = shared.same

-- local newline = function(text)
--   return t { "", text }
-- end

-- local require_var = function(args, _)
--   local text = args[1][1] or ""
--   local split = vim.split(text, ".", { plain = true })

--   local options = {}
--   for len = 0, #split - 1 do
--     table.insert(options, t(table.concat(vim.list_slice(split, #split - len, #split), "_")))
--   end

--   return snippet_from_nodes(nil, {
--     c(1, options),
--   })
-- end

-- return {
--   ignore = "--stylua: ignore",

--   lf = {
--     desc = "table function",
--     "local ",
--     i(1),
--     " = function(",
--     i(2),
--     ")",
--     newline "  ",
--     i(0),
--     newline "end",
--   },

--   -- TODO: I don't know how I would like to set this one up.
--   f = fmt("function({})\n  {}\nend", { i(1), i(0) }),

--   -- test = { "mirrored: ", i(1), " // ", same(1), " | ", i(0) },

--   req = fmt([[local {} = require("{}")]], {
--     d(2, require_var, { 1 }),
--     i(1),
--   }),

--   treq = fmt([[local {} = require("telescope.{}")]], {
--     d(2, require_var, { 1 }),
--     i(1),
--   }),

--   -- test = { "local ", i(1), ' = require("', f(function(args)
--   --   table.insert(RESULT, args[1])
--   --   return { "hi" }
--   -- end, { 1 }), '")', i(0) },

--   -- test = { i(1), " // ", d(2, function(args)
--   --   return snippet_from_nodes(nil, { str "hello" })
--   -- end, { 1 }), i(0) },
-- }
