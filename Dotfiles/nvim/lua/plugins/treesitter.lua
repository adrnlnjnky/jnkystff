-- ###############################################################################
-- ###############################################################################
-- ###                                                                         ###
-- ###                            NVIM CONFIGURATION:                          ###
-- ###                              NVIM-LIGHTBULD                             ###
-- ###              Shows Lightbulb when Code Action is available              ###
-- ###                                                                         ###
-- ###############################################################################
-- ###############################################################################

return {
  "nvim-treesitter/nvim-treesitter",
  opts = {
    ensure_installed = {
      "bash",
      "elixir",
      "html",
      "javascript",
      "json",
      "lua",
      "markdown",
      "markdown_inline",
      "python",
      "query",
      "regex",
      -- "tsx",
      -- "typescript",
      "vim",
      "yaml",
    },
  },
}
