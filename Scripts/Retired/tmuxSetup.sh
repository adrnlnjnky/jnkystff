# script to set up my tmux envirenment

project="groceries"
work_project="wiggy_wanna"

work_base="$HOME/Projects/"
project_base="$HOME/Projects/$project"

project_path=$project_base$groceries
work_path=$work_base$work_project

# Development session
# cd $dev_path
cd $project_path

tmux new-session -s DEV -d
tmux new-window -t DEV -n NOTES -d
tmux split-window -h -t DEV:1
tmux split-window -h -t DEV:1
# tmux split-window -h -t 1
# tmux new-window -t DEV -n SERVER -d
tmux new-window -t DEV -n PLAYGROUND -d

# Config Session
cd ~/.config/nvim
tmux new-session  -s CONFIG -n NVIM -d

cd ~/.config/tmux
tmux new-window  -t CONFIG -n TMUX -d

cd ~/.config/kitty
tmux new-window  -t CONFIG -n KITTY -d

cd ~/.config/zsh
tmux new-window -t CONFIG -n ZSH -d

cd ~/adrnlnjnky/Scripts/
tmux new-window  -t CONFIG -n SCRIPTS -d
cd
cd ~/.config/nvim/
tmux new-session  -s CONFIG -n SNIPPETS -d

# Work Session
cd $work_base
tmux new-session -s WORK -d -n Dev
tmux new-window -t WORK -n SERVER -d
tmux split-window -h -t WORK:1
tmux split-window -h -t WORK:1

cd ~/Projects/exercism/elixir/
tmux new-session -s Exercism -d -n Dev
tmux split-window -h -t Exercism


tmux send-keys -t CONFIG:SNIPPETS. v\ lua/adrnlnjnky/snips/elixir.lua Enter
tmux send-keys -t CONFIG:2. v\ tmux.conf Enter
tmux send-keys -t CONFIG:3. v\ kitty.conf Enter
tmux send-keys -t CONFIG:4. cl Enter
tmux send-keys -t WORK:2. iex -S mix phx.server Enter

cd

tmux attach -t DEV







