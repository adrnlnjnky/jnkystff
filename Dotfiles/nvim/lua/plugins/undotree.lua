-- ###############################################################################
-- ###############################################################################
-- ###                                                                         ###
-- ###                            NVIM CONFIGURATION:                          ###
-- ###                                 TEMPLATE                                ###
-- ###      Async templates into files  with variable and Lua functions        ###
-- ###                                                                         ###
-- ###############################################################################
-- ###############################################################################

return {
  {
    "jiaoshijie/undotree",

    lazy = false,
    cmd = "StartupTime",
    event = "InsertEnter, BufEnter",

    keys = {
      { "<leader>fu", ":undotreeToggle<cr>", { mode = "n", noremap = true, desc = "Undo Tree" } },
    },
  },
}
