--H:##############################################################################
--H:##############################################################################
--H:##                                                                         ###
--H:##                            NVIM CONFIGURATION:                          ###
--H:##                              TODO_COMMENTS                              ###
--H:##                     searchable TODO, HACK and other labels.             ###
--H:##                                                                         ###
--H:##############################################################################
--H:##############################################################################

return {
  {
    "folke/todo-comments.nvim",

    -- QUESTION: any other fun tags we can use?
    -- NOTE: You may want to add a custom mapping to search just the given tag.
    opts = {
      signs = true,
      keywords = {
        QUESTION = { icon = "¿", color = "#FF0A8C" },
        SECTION = { icon = "🚂", color = "#A88525" },
        H = { icon = "💁", color = "#A88525" },
      },
    },
  },
}
