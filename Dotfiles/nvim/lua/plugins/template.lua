-- ###############################################################################
-- ###############################################################################
-- ###                                                                         ###
-- ###                            NVIM CONFIGURATION:                          ###
-- ###                                 TEMPLATE                                ###
-- ###      Async templates into files  with variable and Lua functions        ###
-- ###                                                                         ###
-- ###############################################################################
-- ###############################################################################

-- local user = require("adrnlnjnky.globals").User

-- with lazy.nvim
return {
  {
    "glepnir/template.nvim",
    cmd = { "Template", "TemProject" },
    config = function()
      require("template").setup({
        temp_dir = "/home/adrnlnjnky/.config/nvim/template",
        author = "Tom Peltier",
        email = "adrnlnjnky@gmail.com",
      })
    end,
  },
}
-- lazy load you can use cmd or ft. if you are using cmd to lazyload when you edit the template file
-- you may see some diagnostics in template file. use ft to lazy load the diagnostic not display
-- when you edit the template file.

-- return {
--   { "glepniir/template.nvim" },
--   cmd = { "Template", "TemProject" },
--   config = function()
--     require("template").setup({
--       temp_dir = "/home/adrnlnjnky/.config/nvim/templates",
--       author = "Tom Peltier",
--       email = "adrnlnjnky@gmail.com",
--     })
--   end,
-- }
-- -- return {
-- --   {
-- --     -- "glepnir/template.nvim",
-- --     "adrnlnjnky/template.nvim",
-- --       temp_dir = "/home/adrnlnjnky/.config/nvim/templates",
-- --     opts = {
-- --       author = "Tom Peltier",
-- --       -- author = user,
-- --       email = "adrnlnjnky@gmail.com",
-- --     },
-- --   },
-- -- }
