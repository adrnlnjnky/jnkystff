make latest
cmake latest
neovim nightly
tmux latest
erlang 24.3.4.11
erlang 25.3
erlang latest
elixir 1.14.4
elixir 1.13.4
elixir 1.12.3
elixir latest
nodejs latest
postgres latest
postgres 14.7
postgres 13.10
python latest
lazygit latest
shellcheck latest, https://github.com/luizm/asdf-shellcheck.git
shftmt latest, https://github.com/luizm/asdf-shfmt.git


