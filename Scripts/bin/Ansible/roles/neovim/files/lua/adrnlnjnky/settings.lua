-- *********************************************************""
-- ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^""
-- *^*****************************************************^*""
-- *^*                                                   *^*""
-- *^*  ///////////////////////////////////////////////  *^*""
-- *^*                                                   *^*""
-- *^*  I Like a header for my config files              *^*""
-- *^*                                                   *^*""
-- *^*                                                   *^*""
-- *^*                                                   *^*""
-- *^*                                                   *^*""
-- *^*****************************************************^*""
-- ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^""
-- *********************************************************""



local opt = vim.opt

-- Cool floating popup for completionon command line
--
opt.pumblend = 17
opt.wildmode = "longest:full"
opt.wildoptions = "pum"

opt.cmdheight = 1

opt.showmode = false
opt.showcmd = true
opt.hidden = true
opt.lazyredraw = true
opt.list = false

opt.swapfile = false
opt.linebreak = true

opt.hlsearch = true

opt.cursorcolumn = false
opt.cursorline = false

opt.relativenumber = true
opt.number = true

opt.termguicolors = true

-- "opt.spell spelllang=en-us

opt.textwidth = 120
opt.wrap = false

-- tab stuff
opt.autoindent = true
opt.cindent = true
opt.tabstop = 4
opt.shiftwidth=4
opt.softtabstop=4
opt.expandtab = true

opt.breakindent = true

-- Searching
opt.incsearch = true
opt.ignorecase = true
opt.smartcase = true
opt.showmatch = true
opt.hlsearch = true

-- splits
opt.equalalways = false
opt.splitright = true
opt.splitbelow = true

opt.updatetime = 1000

opt.belloff = "all"


opt.laststatus=1

opt.clipboard = "unnamedplus"
-- opt.clipboard = true
-- opt.emojo = true
-- opt.formatoptions+=false

opt.scrolloff=999


-- augroup highlight_yank
-- 	autocmd!
-- 	au TextYankPost * silent! lua highlight.on_yank{higroup="incSearch", timeout=700}
-- augroup End opt.updatetime=800

-- let g:ypthon3_host_prog = '/usr/bin/python'
-- opt.foldmethod=marker



-- let g:floaterm_width = 0.9
-- let g:floaterm_height = 0.9

-- autocmd BufWritePost *Xresources, *Xdefaults !xrdb %

-- autocmd BufWritePost ~/.config/ninit.vim source %


-- if !exists('g:c_loaded')
--     let g:c_loaded = 1
--     autocmd BufWritePost ~/.config/dotfiles/ninit.vim source %
-- endif

-- if (exists(":terminal"))
--     tnoremap <Esc><Esc> <C-\><C-N>
-- endif

-- autocmd BufNewFile *.md Or ~/Templages/README.md
-- autocmd BufNewFile *.sh 0r ~/Templates/zsh.sh








