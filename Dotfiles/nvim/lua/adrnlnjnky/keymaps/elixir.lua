-- local nmap = require("config.keymaps").nmap
local vmap = require("config.keymaps").vmap
local wk = require("which-key")

wk.register({
  ["<leader>t"] = {
    name = "+Elixir",
    p = { ":ElixirFromPipe<CR>", "To Pipe" },
    b = { ":ElixirFromPipe<CR>", "From Pipe" },
  },
})
vmap({ "<leader>tm", ":ElixirExpandMacro<CR>", { desc = "From Pipe", buffer = true, noremap = true } })
