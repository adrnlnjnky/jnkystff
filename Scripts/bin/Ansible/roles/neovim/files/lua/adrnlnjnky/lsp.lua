-- local imap = require('adrnlnjnky.keymap').imap
-- local nmap = require('adrnlnjnky.keymap').nmap


local has_lsp, lspconfig = pcall(require, 'lspconfig')
if not has_lsp then
  return
end

vim.diagnostic.config({
  virtual_text = false,
  signs = true,
  underline = true,
  update_in_insert = false,
  severity_sort = true,
})

vim.o.updatetime = 250
-- vim.cmd [[autocmd CursorHold * lua vim.diagnostic.open_float(nil, {focus=false})]]
vim.cmd [[autocmd CursorHold,CursorHoldI * lua vim.diagnostic.open_float(nil, {focus=false})]]


local nvim_lsp = require("lspconfig")
local lspkind = require('lspkind')

require('lspkind').init({
    -- enables text annotations
    --
    -- default: true
    -- with_text = true,

    mode = 'symbol_text',
    preset = 'codicons',

    symbol_map = {
      Text = "",
      Method = "",
      Function = "",
      Constructor = "",
      Field = "ﰠ",
      Variable = "",
      Class = "ﴯ",
      Interface = "",
      Module = "",
      Property = "ﰠ",
      Unit = "塞",
      Value = "",
      Enum = "",
      Keyword = "",
      Snippet = "",
      Color = "",
      File = "",
      Reference = "",
      Folder = "",
      EnumMember = "",
      Constant = "",
      Struct = "פּ",
      Event = "",
      Operator = "",
      TypeParameter = ""
    },
})

-- nvim_lsp.jedi_language_server.setup{
--   cmd = {"python", "-m", "jedi-language-server"},
--   on_attach=on_attach_vim_plus_keymaps
-- }

-- :LspInstall pyls_ms
-- nvim_lsp.sumneko_lua.setup{
  -- on_attach=on_attach_vim_plus_keymaps,
-- }

 nvim_lsp.gopls.setup{
   on_attach=on_attach_vim,
   root_dir=nvim_lsp.util.root_pattern('.git');
 }

-- nvim_lsp.tsserver.setup {
--     on_attach =  on_attach,
--     capabilities = capabilities,
-- }

local runtime_path = vim.split(package.path, ';')
table.insert(runtime_path, "lua/?.lua")
table.insert(runtime_path, "lua/?/init.lua")

-- require'lspconfig'.sumneko_lua.setup{}

require'lspconfig'.sumneko_lua.setup{
   cmd = {"/home/adrnlnjnky/.local/share/vim-lsp-settings/servers/sumneko-lua-language-server/sumneko-lua-language-server" },
  settings = {
    Lua = {
      version = 'LuaJIT',
      path = runtime_path,
    },
    diagnostics = {
      globals = {'vim'},
    },
    workspace = {
      library = vim.api.nvim_get_runtime_file("", true),
    },
    telemetry = {
      enable = false,
    },
  },
}

require'lspconfig'.pyright.setup{}

require'lspconfig'.jsonls.setup{}
require'lspconfig'.erlangls.setup{}

require'lspconfig'.elixirls.setup{
   cmd = {"/home/adrnlnjnky/.local/share/vim-lsp-settings/servers/elixir-ls/language_server.sh" },
   -- cmd = {"/home/adrnlnjnky/.local/share/nvim/plugins/lsp/elixir-ls/release/language_server.sh" },
  capabilities = capabilities,
  on_attach = on_attach,
  settings = {
    elixirLS = {
      dialyzerEnabled = true,
      fetchDeps = true
    }
 }
}

-- require 'lspconfig'.tsserver.setup{
--   on_attach = on_attach,
--   filetypes = {
--     "javascript",
--     "javascriptreact",
--     "javascript.jsx",
--     "js",
--     "typescript",
--     "typescriptreact",
--     "typescript.tsx",
--     "ts"
--   }
-- }

require 'lspconfig'.html.setup{
  on_attach = on_attach,
  filetypes = {
    "html",
    -- "heex",
    -- "html.heex",
    -- "eelixir"
  },
  root_dir = function() return vim.loop.cwd() end
}

require'lspconfig'.leanls.setup{}
