-- ###############################################################################
-- ###############################################################################
-- ###                                                                         ###
-- ###                      NVIM COLORSCHEME CONFIGURATION:                    ###
-- ###                                CATPPUCCIN                               ###
-- ###                               blame module                              ###
-- ###                                                                         ###
-- ###############################################################################
-- ###############################################################################

local M = {}

function M.get()
  return {
    -- which key
    WhichKey = { link = "NormalFloat" },
    WhichKeyBorder = { link = "FloatBorder" },
    WhichKeygroup = { fg = c.blue },
    WhichKeySeperator = { fg = c.overlay0 },
    WhichKeyDesc = { fg = c.pink },
    WhichKeyvalue = { fg = C.overlay0 },

    -- Harpooon
    HarpoonWindow = { fg = C.text, bg = O.transparent_background and C.none or C.base },
    HarpoonBorder = { fg = C.blue },
  }
end

return M
