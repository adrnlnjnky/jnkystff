augroup highlight_yank
	autocmd!
	au TextYankPost * silent! lua vim.highlight.on_yank{higroup="incSearch", timeout=700}
augroup End set updatetime=800

let g:ypthon3_host_prog = '/usr/bin/python'
set foldmethod=marker



let g:floaterm_width = 0.9
let g:floaterm_height = 0.9

autocmd BufWritePost *Xresources, *Xdefaults !xrdb %

autocmd BufWritePost ~/.config/nvim.init.vim source %


if !exists('g:vimrc_loaded')
    let g:vimrc_loaded = 1
    autocmd BufWritePost ~/.config/dotfiles/nvim/init.vim source %
endif

if (exists(":terminal"))
    tnoremap <Esc><Esc> <C-\><C-N>
endif

autocmd BufNewFile *.md Or ~/Templages/README.md
autocmd BufNewFile *.sh 0r ~/Templates/zsh.sh








