-- ###############################################################################
-- ###############################################################################
-- ###                                                                         ###
-- ###                            NVIM CONFIGURATION:                          ###
-- ###                               ICON-PICKER:                              ###
-- ###       Pick ALT Font Characters ,Symbols, Nerd Font Icons & Emojis       ###
-- ###                                                                         ###
-- ###############################################################################
-- ###############################################################################

return {
  {
    "ziontee113/icon-picker.nvim",

    opts = {
      cmd = {
        "IconPickerNormal",
        "IconPickerInsert",
        "IconPickerYank",
        "<LEADER>i",
      },
      config = {
        disable_legacy_commands = true,
      },

      --   keys = {
      --     { "<leader>ia", "<cmd>IconPickerNormal<cr>", desc = "Icon Picker Normal All" },
      --     { "<leader>iy", "<cmd>IconPickerYank<cr>", desc = "Icon Picker Yank" },
      --     { "<leader>ii", "<cmd>IconPickerInsert<cr>", desc = "Icon Picker Insert" },
      --   },
    },
  },
}
