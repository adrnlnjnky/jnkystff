--H:##############################################################################
--H:##############################################################################
--H:##                                                                         ###
--H:##                            NVIM CONFIGURATION:                          ###
--H:##                              PERL NAVIGATOR                             ###
--H:##                         LSP for the PERL language                       ###
--H:##                                                                         ###
--H:##############################################################################
--H:##############################################################################

return {
  {
    "bscan/PerlNavigator",
    -- opts = {
    --     config = {
    --         perlnavigator = {
    --             perlPath = "perl",
    --             enableWarnings = true,
    --             perltidyProfile = "",
    --             perlcriticProfile = "",
    --             perlcriticEnabled = true,
    --         },
    --     },
    -- },
  },
}
