-- :###############################################################################
-- :###############################################################################
-- :###                                                                         ###
-- :###                            NVIM CONFIGURATION:                          ###
-- :###                                AUTOCOMANDS                              ###
-- :###    Auto commands that run automatically when given events happen.       ###
-- :###                                                                         ###
-- :###############################################################################
-- :###############################################################################

-- GROUPS
-- ======

vim.api.nvim_create_augroup("scripting", { clear = true })
vim.api.nvim_create_augroup("markdown", { clear = true })

-- : Funtions

-- local function augroup(name)
--   return vim.api.nvim_create_augroup("lazyvim_" .. name, { clear = true })
-- end

-- NEW

-- *****************************************************
-- *****************************************************
-- **                 SCRITPTING                      **
-- *****************************************************
-- *****************************************************

-- vim.api.nvim_create_autocmd({ "BufWrite" }, {
vim.api.nvim_create_autocmd({ "BufNewFile" }, {
  group = "scripting",
  pattern = { "*.sh", "#!/" },
  command = ":!chmod 700 %",
})

vim.api.nvim_create_autocmd({ "BufNewFile" }, {
  group = "scripting",
  pattern = { "*.sh", "#!/" },
  command = "i#!/usr/bin/env elixir",
})

-- *****************************************************
-- *****************************************************
-- **                   MARKDOWN                      **
-- *****************************************************
-- *****************************************************

vim.api.nvim_create_autocmd("VimLeavePre", {
  group = "markdown",
  command = ":%s/https:.*/[&](&)",
})

vim.api.nvim_create_autocmd({ "BufNewFile" }, {
  group = "markdown",
  command = "0r ~/.config/nvim/skeleton/basic.md",
  pattern = "*.md",
})

vim.api.nvim_create_autocmd({ "BufNewFile" }, {
  group = "markdown",
  pattern = "*.md",
  command = ":%s/<<_author_>>/Tom Peltier/",
})

vim.api.nvim_create_autocmd({ "BufNewFile" }, {
  group = "markdown",
  pattern = "*.md",
  command = ":%s/<<_date_>>/\\=system('date')",
})

vim.api.nvim_create_autocmd({ "BufNewFile" }, {
  group = "markdown",
  pattern = "*doctor*.md",
  command = "%s/query: true/query: false",
})

vim.api.nvim_create_autocmd({ "BufWrite" }, {
  group = "markdown",
  pattern = "*doctor*.md",
  command = ":!chmod 600 %",
})

vim.api.nvim_create_autocmd({ "BufNewFile" }, {
  group = "markdown",
  pattern = "*doctor*.md",
  command = "%s/<<_tag_>>/doctor, <<_tag_>>",
})

-- vim.api.nvim_create_autocmd({ "BufWritePre" }, {
--   group = "markdown",
--   pattern = "*.md",
--   command = ":%s/date:/\\=system('date')",
-- })

-- vim.api.nvim_create_autocmd({ "BufWritePre" }, {
--   group = "markdown",
--   pattern = "*.md",
--   command = ":4s/ /date: ",
-- })

-- NOTES
vim.api.nvim_create_autocmd({ "BufNewFile" }, {
  group = "markdown",
  pattern = "*note*.md",
  command = ":%s/<<_tag_>>/note, <<_tag_>>",
})

vim.api.nvim_create_autocmd({ "BufNewFile" }, {
  group = "markdown",
  pattern = "*note*.md",
  command = ":%s/<<_title_>>/NAME Notes",
})
-- From the templating pluging that's giving fit's so I'm writing my own
-- autcomands.
-- -- Markdown Basic

-- -- **************
-- vim.api.nvim_create_autocmd({ "BufNewFile" }, {
--   group = "markdown",
--   command = ":Template basic.md",
--   pattern = "*.md",
-- })

-- vim.api.nvim_create_autocmd({ "BufWritePre" }, {
-- Update Date
--   group = "markdown",
--   command = "echo ggjjjj<leader><CTL>o<CTL>o",
--   pattern = "*.md",
-- command = "echo <leader>d",
-- })

-- FT FILES
-- *****************
vim.api.nvim_create_augroup("ftTemplates", { clear = true })

-- BASH
vim.api.nvim_create_autocmd({ "BufNewFile" }, {
  group = "ftTemplates",
  pattern = "bash.md",
  command = ":Template bash",
})

-- FISH
vim.api.nvim_create_autocmd({ "BufNewFile" }, {
  group = "ftTemplates",
  pattern = "fish.md",
  command = ":Template fish",
})

-- HTML
vim.api.nvim_create_autocmd({ "BufNewFile" }, {
  group = "ftTemplates",
  pattern = "html.md",
  command = ":Template html",
})

-- PERL
vim.api.nvim_create_autocmd({ "BufNewFile" }, {
  group = "ftTemplates",
  pattern = "perl.md",
  command = ":Template perl",
})

vim.api.nvim_create_autocmd({ "BufNewFile" }, {
  group = "markdown",
  pattern = "*.md",
  command = ":%s/, <<_tag_>>//",
})
