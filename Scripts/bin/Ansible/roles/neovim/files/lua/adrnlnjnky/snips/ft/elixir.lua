local ls = require("luasnip")
-- some shorthands...
local s = ls.snippet
local sn = ls.snippet_node
local t = ls.text_node
local i = ls.insert_node
local f = ls.function_node
local c = ls.choice_node
local d = ls.dynamic_node
local r = ls.restore_node
local l = require("luasnip.extras").lambda
local rep = require("luasnip.extras").rep
local p = require("luasnip.extras").partial
local m = require("luasnip.extras").match
local n = require("luasnip.extras").nonempty
local dl = require("luasnip.extras").dynamic_lambda
local fmt = require("luasnip.extras.fmt").fmt
local types = require("luasnip.util.types")
local conds = require("luasnip.extras.expand_conditions")
-- local ts_locals = require "nvim-treesiter.locals"

local snippets, autosnippets = {}, {}


-- vim.treesitter.set_querry(
--   "elixir",
--   "Luasnip_Result",
--   [[ [
--     (method_declaration result: (_) @id)
--     (function_declaration result: (_) @id)
--     (func_literal result: (_) @id)
--   ]]]
-- )

-- Support

-- Make sure to not pass an invalid command, as io.popen() may write over nvim-text.
local function bash(_, _, command)--{{{
	local file = io.popen(command, "r")
	local res = {}
	for line in file:lines() do
		table.insert(res, line)
	end
	return res
end

function Capitalize(str)
  return (str:gsub("^%l", string.upper))
end

function Split(snip, delimiter)
  local result = {};
  for match in (snip..delimiter):gmatch("(.-)"..delimiter) do
    table.insert(result, match);
  end
  return result;
end


local to_CamelCase
to_CamelCase = function(word)
  local words = Split(word, "_")

  return Capitalize(words[1] .. Capitalize(words[2]))
end

local path = bash(bash, {}, 'realpath " "')
local split = Split(path[1], '/')
local getWord = (split[#split-1])
local fixCase = to_CamelCase(getWord .. "_")

local baseB = fixCase
local baseW = fixCase .. "Web"

-- Option BUILDERS

local def_options
def_options = function()
  return sn(nil, {
    c(1, { t("defp"), t("def") })
  })
end

local map_partr
map_partr = function()
  return sn(nil, {
    c(1, {
      t(""),
      sn(nil, { t(", "), i(1, "other"), t(": "), rep(1), d(2, map_partr, {}) })
    })
  })
end

local map_part
map_part = function()
  return sn(nil, { i(1, "stuff"), t(": "), rep(1), d(2, map_partr, {}) })
end

local map_parts
map_parts = function()
  return
    sn(1, { t("%{"), d(1, map_part, {}), t"}"}), t("COOL")
end

local variable_options
variable_options = function()
 return sn(nil, {
   c(1, { t("_"), t("[]]"), d(1, map_parts, {}) }),
   -- c(1, { t("_"), t("[]]"), d(map_parts, {}) }),

  })
end

local rec_var
rec_var = function()
  return sn(
    nil,
    c(1, { d(1, variable_options, {}), sn(nil, { t(", "), i(1), d(2, rec_var)}), })
  )
end

-- local rec_var
-- rec_var = function()
--   return sn(
--     nil,
--     c(1, { t("_"), sn(nil, { t(", "), i(1), d(2, rec_var)}), })
--   )
-- end

local type_options = function(args, _, _)
  return sn(
    nil,
    c(1, { t("string"), t( args[1] ), t("boolean"), t("decimal"), t("integer"), t("id"), i(1) })
  )
end

local module_options = function()
  return sn(nil, {
          c(1, {
            t{"Absinthe"},
            t(baseB),
            t(baseW),
            t("Phoenix"),
            t("Plug"),
            t("XmlSchema"),
            i(1)
          }),
  })
end

local alias_options
alias_options = function()
  return sn (
    nil, {
      c(1, {
        t(""),
        sn(nil, {
          c(1, {
            t("alias "),
            t("import "),
            t("require "),
            t("")
          }),
          d(2, module_options, {}),
          i(3),
          t({"", "  "}),
          d(4, alias_options, {})
        })
      })
    })
end

-- local defstruct_snip = function()
--   return sn(nil, {
--     t("defstruct ["),
--     i(1),
--     t("]")
--   })
-- end


-- Schema Helper
local args_list
args_list  = function()
  return sn(
  nil,
  c(1, {
    t(""),
    sn(nil, {
      c(1, { t("arg "), t("middleware ") }),
      t(":"),
      i(2, "var"),
      t(", :"),
      d(3, type_options, {2, 1, 2}),
      t({"", "    "}),
      d(4, args_list, {})
    })
  })
  )
end

local module_first_opts
module_first_opts = function()
  return sn (
  nil, {
    c(1, {
      t(""),
      sn(1, {
        c(1, {
          t( { '@moduledoc """', "", "  "  }),
          t( "use " ),
          t( { "defstruct [", "", "" }),
          t( "@behavior" ),
          t( "" ),
        }),
        i(2),
        m(1, "defstruct", "\n  ]", ""),
        m(1, '@moduledoc """', '\n\n  """', ""),
        t({"", ""}),
        d(3, module_first_opts, {}),

      }),
    })
  })
end

-- When you want a function to finish with the option to start a new one
local new_func_options
new_func_options = function()
    return sn(nil, { c(1, { t("def"), t("schema"), t("resolver"), t("") }) })
end

-- Schema Helper: arg builder
local rec_arg
rec_arg = function()
	return sn(
		nil,
		c(1, {
			-- Order is important, sn(...) first would cause infinite loop of expansion.
			t(""),
			sn(nil, {
              t({ "", "\targ :" }),
              i(1, "var"),
              c(2, { t(", non_null( :"), t(", ") }),
              c(3, { t("string"), t("boolean"), t("decimal"), t("integer"), t("id"), i(5) }),
              m(2, ", non_null( :", " )", ""),
              d(4, rec_arg, {})
            }),
		})
	)
end

ls.add_snippets("elixir", {


  s( { trig = "defm" },--{{{
  fmt(
  [[
  defmodule {1}.{2} do
    {3}
    {4}
    {5}{6}

  end
  ]],
  {
    c(1, { t(baseB), t(baseW) }),
    i(2, "ModuleName"),
    d(3, module_first_opts, {}),
    d(4, alias_options, {}),
    d(5, new_func_options, {}),
    i(6)
  })),

	s( "def", fmt(
[[
{1} {2}({3}{4}) do
  {5}{6}
   {7}
   {8}{}

end
]],
	{
      d(1, def_options, {}),
      i(2, "func_name"),
      d(3, variable_options, {}),
      d(4, rec_var, {}),
      c(5, {
        t(""),
        i(1),
        sn(2, { i(1, "var"), t(" =") }),
      }),
      m(5, "[a..z]", " =", ""),
      rep(3),
      c(6, {
        t(""),
        sn(1, {
          c(1, { t("|> "), t("")}),
          c(2, {
            t(""), t("Enum"), t("List"), t("String"), t("Map"),
            t("Integer"), t("Float"),
            t("File"), t("IO"),
            t("Range"),
            t("Agent"), t("Tuple"), t("Time")
          }),
          i(3),
        }),
      }),
      -- i(0)
      d(7, new_func_options, {}),
	})
	),--}}}

  s("if", {--{{{
    t("if "),
    i(1, "arg"),
    t(" == "),
    i(2, "true"),
    t(", do "),
    i(3, "action_for"),
    i({0}),
    t(", else "),
    i(4, "action_otherwise"),
    t("end"),
    t(""),
  }
  ),

  s("ifs", {
    t("if "),
    i(1, "arg"),
    t(" == true, do: "),
    i(2, "todo"),
    t(", else: "),
    i(3, "or_do"),
    t(""),
    t(""),
    i({0}),
  }
  ),

    s( "for", fmt(
    [[
    for {2} <- {1} do
      {3}
    end

    ]],
    {
      i(1, "enumerable"),
      i(2, "item"),
      dl(3, l._2, 2),
    }
    )
    ),--}}}

    s("field", fmt(--{{{
    [[
    @desc "{1}"
    field :{2}, :{3} do
      {4}

    resolve &{5}{6}
  end

  {7}
  ]],{
    i(1, "---> Description <---"),
    i(2, "schema_name"),
    -- TODO: dynamicaly find the object names in the file.
    c(3, { t("string"), t("boolean"), t("decimal"), t("integer"), i(2) }),
    -- i(4),
    d(4, args_list, {}),
    c(5, { t("Resolvers"), i(1) }),
    i(6, ""),
    i(7),
  })),--}}}


    s( "resolve", fmt(--{{{{{{{{{{{{
    [[
    def {1}({2}, {3}, {4}) do
      {6}
    {{:ok, {5}}}

    end

    {7}
    ]],{
      i(1, "func_name"),
      i(2, "_root"),
      i(3, "_args"),
      i(4, "_info"),
      c(5, { t("To_RETURN"), i(5) }),
      i(6, "-- TODO -> work goes here"),
      c(7, { i(7), t("res"), t("def") }),

    })
    ),--}}}}}}}}}}}}

    s( "resa", fmt(--{{{
    [[
    def {1}({2}, {3}, {4}) do
      case {5}({6}) do
        {7} -> {8}
        {9} -> {10}
      end
    end

    {11}
    ]],{
      i(1, "func_name"),
      i(2, "_root"),
      i(3, "_args"),
      -- c( { i(3, "args"), t("_args") }),
      i(4, "_info"),
      i(5, "ToImprove"),
      rep(3),
      i(6),
      i(7),
      c(8, { t("_error"), t(":error"), t("_"), i(8) }),
      i(9, "{:error, \"Some error\"}"),
      i(10)

    })
    ),

})--}}}

ls.add_snippets("elixir", {--{{{


  s("defws", {
    c(1, { t("def "), t("defp ") }),
    i(2, "func_name"), t("("), i(3, "arg"), t(") when "),
    rep(3), t(" "),
    c(4, { t(""), t("== "), t("!= "), t("<= "), t("< "), t(">= "), t("> "), t("is in "), i(1) }),
    m(4, "is in ", "[", ""),
    i(5, "something"),
    m(4, "is in ", "]", ""),
    t({" do", ""}),
    i(0),
    t({"", "end"})
  }),--}}}

  s("defwd", {--{{{
    c(1, { t("defp "), t("def ") }),
    i(2, "func_name"), t("("), i(3, "arg"), t(", "), i(4, "acc"), t(") when "),
    rep(3), t(" "),
    c(5, { t(""), t("== "), t("!= "), t("<= "), t("< "), t(">= "), t("> "), t("is in "), i(1) }),
    m(5, "is in ", "[", ""),
    i(6, "qualifier"),
    m(5, "is in ", "]", ""),
    t({" do", ""}),
    t("  "),
    i(0),
    t({"", "  "}),
    rep(2), t("("), rep(3), t(", "), rep(4),
    t({")", "end"})
  }),

  s("defwf", {
    c(1, { t("defp "), t("def ") }),
    i(2, "func_name"), t("("),
    c(3, { t("[]"), t("%{}"), t(""), t({}), i(1)}),
    t(", "),
    c(4, {t("acc"), i(1)}),
    t("), do: "),
    rep(4), t({"", ""}),
    i(0)
  }),

	s( "defstart", fmt(
[[
{1} {2}({3}) do
end


]],
			{
                c(1, {
                t("def "),
                t("defp "),
                }),
                i(2, "start_link"),
		        i(3, " opts \\ [] "),
			}
		)
	),--}}}

	s( "defsum", fmt(--{{{
[[
{1} {2}({3}, {4}) do
   {5} = {7} + {8}

   {6}
end

{9}


]],
			{
                c(1, {
                t("def "),
                t("defp "),
                }),
                i(2, "sum"),
		        i(3, "60"),
				i(4, "9"),
                i(5, "total"),
                rep(5),
                rep(3),
                rep(4),
                i(0)

			}
		)
	),
--}}}
	s( "defindex", fmt(--{{{
[[
{1} {2}({3}, {4}, {5}) do

  {7} =
    {6}
    |> {9}

  {8}
end


]],
			{
                c(1, {
                t("def "),
                t("defp "),
                }),
                -- i(2),
                c(2, {t("mount"), t("handle_parms"), t("handle_info"), t("handle_event"), t("apply_action"), t("index"), i(2, "func_name")  }),
		        i(3, "_params"),
				i(4, "_session"),
                i(5, "socket"),
				rep(5),
				rep(5),
                -- t(":ok, socket}")
                c(6, { t("{:ok, socket}"), t("") }),
                i(0)
			}
		)
	),
--}}}
    s( "deffor", fmt(--{{{
[[
{1} {2}({3}, {4}) do

  for {5} <- {6} do
    if {7} == {8}, do: {9}
  end
end

]],
{
    c(1, { t("defp "), t("def "), }),
    i(2, "get_list"),
    i(3, "list"),
    i(4, "thing"),
    i(5, "item"),
    rep(3),
    rep(5),
    rep(4),
    rep(5),
  }

  )
),--}}}

    -- s( "fieldq", fmt(--{{{
    -- [[
    -- field :{1}, :{2} do
    --   resolve fn {3}, {4}, {5}, ->
    --     case {6} do
    --       {7}{8}{9} ->
    --         {10}{11}{12}
    --       {13} ->
    --         {14}{15}{16}
    --     end
    --   end
    -- end
    -- ]],{
    --   i(1, "origin"),
    --   i(2, "string"),
    --   i(3, "_source"),
    --   i(4, "_"),
    --   i(5, "_args"),
    --   rep(5),

    --   t("{:ok, "),
    --   t("To_Match"),
    --   t("}"),

    --   t("{:ok, "),
    --   c(6, { t("To_Return"), i(7) }),
    --   t("}"),

    --   c(7, { t("_"), t(":error") }),

    --   t("{:error, "),
    --   c(8, { t("Some error message"), t("Some other Error message") }),
    --   t("}")
    -- })),--}}}

    -- s("fieldx", fmt(--{{{
    -- [[
    -- @desc "{9}"
    -- field :{1}, :{2} do
    -- arg:({3}, {4}(:{5})){6}

    -- resolve(${7}{8})
  -- end

  -- {10}
  -- ]],{
    -- i(1, "schema_name"),
    -- d(2, type_options, {}),
    -- i(3, "id"),
    -- c(4, { t("non_null"), t("") }),
    -- d(5, type_options, {}),
    -- d(6, rec_arg, {}),
    -- c(7, { t("Resolver."), t("Resolvers."), t(baseB), t(baseW), i(1) }),
    -- rep(1),
    -- i(8),
    -- i(0)
  -- })),--}}}


  s("resolverr",
  {
    t("def "),
    i(1, "func_name"),
    t("("),
    c(2, { t("_root"), t("root"), d(1, map_parts, {}) }),
    t(", "),
    c(3, { t("_args"), t("args"), d(1, map_parts, {}) }),
    t(", "),
    c(4, { t("_context"), t("context"), d(1, map_parts, {}) }),
    t( {") do", "" }),
    i(0),
    t({ "", "end"})


  }),

  s("dl1", {
    i(1, "sample_text"),
    i(2, "sample_text_2"),
    t({ ":", ""}),
    dl(3, l._1:gsub("\n", " linebreak ") .. l._2, {1, 2 }),
  }),


    s("schemaa", fmt(--{{{
    [[
    @desc "{4}"
    field :{1}, :{2} do
      {3}

    resolve &{5}{6}
  end

  {7}
  ]],{
    i(1, "schema_name"),
    -- c(2, { t("string"), dl(1, l._1, 1), t("boolean"), t("decimal"), t("integer") ,i(2) }),
    -- c(2, { t("string"), t("boolean"), t("decimal"), t("integer") ,i(1) }),
    d(2, type_options, { 1, 1, 1 }),
    -- i(4),
    d(3, args_list, {}),
    i(4, "---> Description <---"),
    c(5, { t("Resolvers"), i(1) }),
    i(6),
    i(7),
  })),--}}}

  s("handlefunc",
  t("hello"),
  i(1)
  ),
},
{
	type = "autosnippets",
	-- key = "all_auto",
  })






return snippets, autosnippets

















