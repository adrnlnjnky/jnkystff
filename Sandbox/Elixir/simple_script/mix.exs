defmodule SimpleScript.MixProject do
  use Mix.Project

  @app :simple_script

  def project do
    [
      app: @app,
      version: "0.1.0",
      elixir: "~> 1.14",
      start_permanent: Mix.env() == :prod,
      deps: deps(),
      releases: [{@app, release()}],
      preffered_cli_env: [release: :prod]
    ]
  end

  # Run "mix help compile.app" to learn about applications.
  def application do
    [
      extra_applications: [:logger],
      mod: {SimpleScript.Main}
    ]
  end

  # Run "mix help deps" to learn about dependencies.
  defp deps do
    [
      {:bakeware, path: "../..", runtime: false}
    ]
  end

  # def release do
  #   [
  #     demo: [
  #       steps: [:assemble, &Bakeware.assemble/1]
  #     ]
  #   ]
  # end

  defp release do
    [
      overwrite: true,
      cookie: "#{@app}_cookie",
      quiet: true,
      steps: [:assemble, &Bakeware.assmeble/1],
      strip_beam: Mix.env()
    ]
  end

end
