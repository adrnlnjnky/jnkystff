defmodule Loopy.BackSeat do
  @behaviour Loopy

  def go do
    Loopy.run(__MODULE__)
  end

  @impl true
  def work do
    [
      "Ow\n",
      "quit IT\n",
      "are we there yet\n",
      "let's go see rock city\n",
      "I'm Hungry\n",
      "I'm thursty\n"
    ]
    |> Enum.random()
    |> IO.puts()
  end

  @impl true
  def wait do
    5_000
    |> :rand.uniform()
    |> Process.sleep()
  end
end
