local M = {}

function M.setup()
	require('telescope').setup{
		defaults = {
			sorting_stategy = "ascending",
			-- prompt-position = "top",
			show_pluto = true,
			layout_strategy = 'vertical',
			shorten_path = true,
			preview_cutoff = 40,
			winblen = 0,
                        borderchars = { '─', '│', '─', '│', '╭', '╮', '╯', '╰'},

		--	layout_strategy = 'flex',
		--	:echo columns
	}
}
end

-- local actions = require("telescope.actions")
-- local trouble = require("trouble.providers.telescope")

local telescope = require("telescope")

telescope.setup {
  defaults = {
    mappings = {
      -- i = { ["<c-t>"] = trouble.open_with_trouble },
      -- n = { ["<c-t>"] = trouble.open_with_trouble },
    },
  },
}

require('telescope').load_extension('fzy_native')

function M.current_file_cwd()
	return vim.fn.expand("%:h")
end

function M.cfwd ()
	require('telescope.builtin').git_files {
		shorten_path = false,
		cwd = M.current_file_cwd(),
		prompt = "~ CFWD ~"
	}
end
return M

