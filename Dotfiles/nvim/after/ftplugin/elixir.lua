-- ###############################################################################
-- ###############################################################################
-- ###                                                                         ###
-- ###                            NVIM CONFIGURATION:                          ###
-- ###                                   ELIXIR:                               ###
-- ###                     Elixir File Type Only Configuration                 ###
-- ###                                                                         ###
-- ###############################################################################
-- ###############################################################################

local nmap = require("config.keymaps").nmap
local ls = require("luasnip")
local s = ls.snippet
local sn = ls.snippet_node
local t = ls.text_node
local i = ls.insert_node
local c = ls.choice_node
local d = ls.dynamic_node
local rep = require("luasnip.extras").rep
local m = require("luasnip.extras").match
local fmt = require("luasnip.extras.fmt").fmt
local l = require("luasnip.extras").lambda
local isn = ls.indent_snippet_node
-- local f = ls.function_node
-- local r = ls.restore_node
-- local p = require("luasnip.extras").partial
-- local n = require("luasnip.extras").nonempty
-- local dl = require("luasnip.extras").dynamic_lambda
-- local types = require("luasnip.util.types")
-- local conds = require("luasnip.extras.expand_conditions")
-- local ts_locals = require("nvim-treesiter.locals")

local snippets, autosnippets = {}, {} --}}}

-- SECTION: keymaps
-- Testing
-- nmap({
--   "<leadertn",
--   ":!tmux send-keys -t TESTING cl Enter mix test Enter<CR><CR>",
--   { desc = "Send test to 'TESTING' session" },
-- })

-- SECTION: FUNCTIONS & VARIABLES

-- Make sure to not pass an invalid command, as io.open() may write over nvim-text.
local function bash(_, _, command)
  local file = io.popen(command, "r")
  local res = {}
  for line in file:lines() do
    table.insert(res, line)
  end
  return res
end

function Capitalize(str)
  return (str:gsub("^%l", string.upper))
end

function Split(snip, delimiter)
  local result = {}
  for match in (snip .. delimiter):gmatch("(.-)" .. delimiter) do
    table.insert(result, match)
  end
  return result
end

local size = function()
  local count = 0
  for _ in pairs(table) do
    count = count + 1
  end
  return count
end

local to_CamelCase
to_CamelCase = function(word)
  local words = Split(word, "_")

  return Capitalize(words[1] .. Capitalize(words[2]))
end

local from_path
from_path = function(word)
  local words = Split(word, "/")
  local that = ""
  for partCount = 1, #words do
    local this = to_CamelCase(words[partCount] .. "_")
    that = that .. "." .. this
  end
  return that:gsub(".Lib.", "")
end

-- VARIABLES
local path = bash(bash, {}, 'realpath " "')
local split = Split(path[1], "/")
-- local getWord = (split[#split - 1])
-- local getWord = (split[#split - 2])
local getWord = split[#split - 3]
local fixCase = to_CamelCase(getWord .. "_")

local baseB = fixCase
local baseW = fixCase .. "Web"

local buffer = vim.fn.expand("%")
local buffer2 = Split(buffer, ".ex")
local buffer4 = from_path(buffer2[1])
local module = buffer4

-- Option BUILDERS

local module_options = function()
  return sn(nil, {
    c(1, {
      t(""),
      t(baseB),
      t(baseW),
      t({ "Ecto" }),
      t({ "Absinthe" }),
      t("Plug"),
      t("Phoenix"),
      t("XmlSchema"),
      i(1),
    }),
  })
end

local module_first_opts
module_first_opts = function()
  return sn(nil, {
    c(1, {
      t(""),
      sn(nil, {
        c(1, {
          t("use "),
          -- t({ '@moduledoc """', "", "  " }),
          t({ "defstruct [", "", "" }),
          t("@behavior"),
          t(""),
        }),
        i(2),
        m(1, "defstruct", "\n  ]", ""),
        m(1, '@moduledoc """', '\n\n  """', ""),
        t({ "", "" }),
        d(3, module_first_opts, {}),
      }),
    }),
  })
end

local def_options = function()
  return sn(nil, {
    c(1, { t("defp"), t("def") }),
  })
end

-- When you want a function to finish with the option to start a new one
local new_func_options
new_func_options = function()
  return sn(nil, { c(1, { t("def"), t(""), t("schema"), t("resolver"), t("mount") }) })
end

-- local func_options
-- func_options = function()
--   -- return sn(nil, { c(1, { t("def"), t("schema"), t("resolver"), t("") }) })
--   return sn(nil, { c(1, { t("def"), t("schema"), t("resolver"), t("") }) })
-- end

local map_partr

map_partr = function()
  return sn(nil, {
    c(1, {
      t(""),
      sn(nil, { t(", "), i(1, "key"), t(": "), rep(1), d(2, map_partr, {}) }),
    }),
  })
end

local map_part = function()
  return sn(nil, { i(1, "id"), t(": "), rep(1), d(2, map_partr, {}) })
end

local map_parts = function()
  return sn(nil, {
    c(1, {
      t("%{}"),
      sn(nil, {
        t("%{"),
        d(1, map_part, {}),
        t("}"),
        c(2, { t(" = args"), t(" = socket"), i(1), t(" = params") }),
      }),
    }),
  })
end

local variable_options_r
variable_options_r = function()
  return sn(nil, {
    c(1, {
      t(""),
      sn(nil, { t(", "), c({ i(1), t("[]"), d(2, variable_options_r, {}) }) }),
    }),
  })
end

local type_options = function(args, _, _)
  return sn(nil, c(1, { t("string"), t(args[1]), t("boolean"), t("decimal"), t("integer"), t("id"), i(1) }))
end

local alias_options
alias_options = function()
  return sn(nil, {
    c(1, {
      t(""),
      sn(nil, {
        c(1, {
          t("alias "),
          t("import "),
          t("require "),
          t(""),
        }),
        d(2, module_options, {}),
        i(3),
        t({ "", "  " }),
        d(4, alias_options, {}),
      }),
    }),
  })
end

local args
args = function()
  return sn(
    nil,
    c(1, {
      t(""),
      sn(nil, {
        c(1, { t("arg "), t("middleware ") }),
        t(":"),
        i(2, "field"),
        t(", :"),
        d(3, type_options, { 2, 1, 2 }),
        t({ "", "    " }),
        d(4, args, {}),
      }),
    })
  )
end

-- SECTION: LUASNIP: Snippets
ls.add_snippets("elixir", {

  s("testingg", {
    t(fixCase),
    t("baseW: "),
    t({ baseW, "baseB: " }),
    t({ baseB, "Module: " }),
    t(module),
  }),

  s("renderr", {
    t("def render("),
    i(1, "assigns"),
    t({ ") do", "", "  " }),
    i(0),
    t({ "", "", "end" }),
  }),

  s("mountt", {
    t({ "def mount(" }),
    c(1, { t("params, "), t("_params, ") }),
    c(2, { t(""), t("_") }),
    t({ "session, socket) do", "" }),
    t({ "", "" }),
    c(3, { t(""), t({ "socket", "" }) }),
    m(3, "socket", "|> "),
    i(0),
    t({ "", "" }),
    t({ "", "{:ok, socket}" }),
    t({ "", "", "end" }),
    t({ "", "" }),
    t({ "", "" }),
    i(0),
  }),

  s("transform", {
    i(1, "initial text"),
    t("::"),
    i(2, "replacement for e"),
    t({ "", "" }),
    l(l._1:gsub("e", l._2), { 1, 2 }),
  }),

  s("start)", {
    t("def "),
    c(1, { t("start("), t("start_link(") }),
    c(2, { t("params"), i(1, "_params") }),
    t(","),
    i(3, " socket"),
    t(") do"),
    t({ "", "  " }),
    i(0),
    t({ "", "", "" }),
    m(3, "socket", "{:ok, socket}"),
    t({ "", "end " }),
  }),

  -- SECTION: Module Startup snippets

  s("defmm", {
    t("defmodule "),
    t(baseB),
    t(module),
    t({ " do", "  " }),
    t("@moduledoc "),
    c(1, { t({ '""""', "    " }), t({ "false", "  " }) }),
    i(2),
    t({ "", "  " }),
    m(1, '""""', '"""'),
    t({ "", "" }),
    d(3, module_first_opts, {}),
    t({ "", "" }),
    d(4, alias_options, {}),
    t({ "", "" }),
    d(5, new_func_options, {}),
    i(6),
    t({ "", "" }),
    i(0),
    t(""),
    -- t({ "", "", "" }),
    t({ "", "", "end" }),
  }),

  s("defm1", {
    t("defmodule "),
    t(baseB),
    t(module),
    t({ " do", "  " }),
    d(1, module_first_opts, {}),
    t({ "", "" }),
    d(2, alias_options, {}),
    t({ "", "" }),
    d(3, new_func_options, {}),
    i(4),
    t({ "", "", "" }),
    i(0),
    t({ "", "", "end" }),
  }),

  s("defmt", {
    t("defmodule "),
    t(baseB),
    t(module),
    t({ " do", "  " }),
    t({ "  use ExUnit.Case", "" }),
    t({ "", '  description "' }),
    i(1),
    t({ '" do', "" }),
    t({ "", '  test "' }),
    i(2),
    t({ '" do', "", "  " }),
    c(3, { t("assert"), t("refute"), i(1) }),
    t(" "),
    i(4),
    t({ "", "" }),
    t({ "", "  end", "" }),
    i(0),
    t({ "", "end" }),
  }),

  -- SECTION END:
  -- ##########################

  s(
    "defpipe",
    fmt(
      [[
 {1} {2}({3}{4}) do
   {5}
    {6}
    {7}{}

 end
 ]],
      {
        d(1, def_options, {}),
        i(2, "func_name"),
        i(3),
        -- d(3, variable_options_r, {}),
        c(4, {
          t(""),
          i(1),
          sn(2, { i(1, "var"), t(" =") }),
        }),
        m(4, "[a..z]", " =", ""),
        rep(3),
        c(5, {
          t(""),
          sn(1, {
            c(1, { t("|> "), t("") }),
            c(2, {
              t(""),
              t("Enum"),
              t("List"),
              t("String"),
              t("Map"),
              t("Integer"),
              t("Float"),
              t("File"),
              t("IO"),
              t("Range"),
              t("Agent"),
              t("Tuple"),
              t("Time"),
            }),
            i(3),
          }),
        }),
        -- i(0)
        d(6, new_func_options, {}),
      }
    )
  ),

  s("defrs", {
    t("def "),
    i(1, "recusive_func"),
    t("("),
    c(2, { t("[]"), t("list"), i(1), d(2, map_parts, {}) }),
    t(", "),
    c(3, { t("args"), t("[]"), d(1, map_parts, {}) }),
    t({ "), do: args" }),
    t({ "", "" }),
    t({ "", "def " }),
    rep(1),
    t("("),
    i(4, "list"),
    t(", "),
    rep(3),
    t({ ") do", "" }),
    i(5),
    t({ "", "" }),
    rep(1),
    t("("),
    rep(4),
    t(", "),
    rep(3),
    t(")"),
    t({ "", "end", "" }),
    t({ "", "def " }),
    rep(1),
    t("("),
    rep(4),
    t(", "),
    rep(3),
    t("), do: "),
    rep(1),
    t("("),
    rep(4),
    t(", "),
    rep(3),
    t(")"),
    t({ "", "" }),
    i(0),
  }),

  s("handleresponse", {
    d(1, def_options, {}),
    t(" handle_response("),
    c(2, { t("root, "), t("response, ") }),
    m(2, "root, ", "response, "),
    m(2, "response, ", "root, "),
    t("context"),
    t({ ") do", "", "  " }),
    i(0),
    t({ "", "" }),
    t("end"),
  }),

  s("defh", {
    t("def handle_"),
    c(1, { t("request"), t("response"), i(1), t("event"), t("params"), t("call"), t("cast"), t("info") }),
    t("("),
    c(2, { rep(1), t("root"), t("args"), t("mgs"), t("_"), t("[]"), d(1, map_parts, {}) }),
    t(", "),
    c(3, { rep(1), m(2, "args", "root", "args"), t("params"), t("_"), d(1, map_parts, {}) }),
    t(", "),
    c(4, { t("context"), t("socket"), t("_context"), t("[]"), t("{}"), d(1, map_parts, {}) }),
    m(2, "args", "args\n|>", ""),
    i(0),
    t({ "", "", "  " }),
    c(5, { t("{:ok, "), t("{:error, "), t("{:reply, "), t("{:noreply, "), t("") }),
    c(6, { m(4, "socket", "socket", "args"), i(1), m(3, "context", "context", ""), m(3, "params", "params", "") }),
    m(5, "{", "}"),
    t({ "", "end" }),
  }),

  s("resolverr", {
    t("def "),
    i(1, "func_name"),
    t("("),
    c(2, { t("root"), t("_root"), d(1, map_parts, {}) }),
    t(", "),
    c(3, { t("args"), t("_args"), d(1, map_parts, {}) }),
    t(", "),
    c(4, { t("context"), t("_context"), d(1, map_parts, {}) }),
    t({ ") do", "" }),
    i(0),
    t({ "", "end" }),
  }),

  s(
    "schemaa",
    fmt(
      [[
     @desc "{4}"
     field :{1}, :{2} do
       {3}

     resolve &{5}.{6}
   end

   {7}
   ]],
      {
        i(1, "schema_name"),
        d(2, type_options, { 1, 1, 1 }),
        -- i(4),
        d(3, args, {}),
        i(4, "---> Description <---"),
        c(5, { t("Resolvers"), i(1) }),
        rep(1),
        -- i(6),
        i(7),
      }
    )
  ), --}}}

  s("inspp", {
    t("IO.inspect("),
    i(1, "thing"),
    t(', pretty: true, label: "'),
    rep(1),
    t({ '")', "" }),
    i(0),
  }),

  s("inspectit", {
    t('IO.inspect({"||---->>",  '),
    i(1),
    t(", pretty: true, label: "),
    rep(1),
    t("})"),
    t({ "", "" }),
    i({ 0 }),
  }),
}, {
  type = "autosnippets",
  -- key = "all_auto",
})

return snippets, autosnippets
