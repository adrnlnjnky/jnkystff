-- ###############################################################################
-- ###############################################################################
-- ###                                                                         ###
-- ###                            NVIM CONFIGURATION:                          ###
-- ###                                 TEMPLATE                                ###
-- ###      Async templates into files  with variable and Lua functions        ###
-- ###                                                                         ###
-- ###############################################################################
-- ###############################################################################

return {
  { "prettier/vim-prettier" },
}
