defmodule Update do
  @location "/home/adrnlnjnky/.secrets/passwords.txt"
  def update() do
    file = File.read!(@location)

    lines = String.split(file, "\n", trim: true)

    hash_list =
      for line <- lines do
        hash_pass =
          line
          |> String.split(",", trim: true)
          |> Enum.at(1)
          |> Base.encode64()

        line
        |> String.split(",", trim: true)
        |> List.replace_at(1, hash_pass)
        |> Enum.intersperse(",")
        |> List.insert_at(-1, "\n")
        |> List.to_string()

        # |> IO.inspect()
      end

    hash_file =
      hash_list
      |> List.to_string()

    File.write(@location, hash_file)
    IO.puts("DONE")
  end
end

Update.update()
