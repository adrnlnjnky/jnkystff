--       %%%   %%%   %%%%%%%%   %%%   %%%  %%%   %%%       %%%     %%%%%%%%    %%%%%%%
--      %%%   %%%  %%%        %%%   %%% %%%+: %%%+:    %%% %%%   %%%    %%% %%%    %%%
--     %%%  %%%   %%%         %%% %%% %%% %%%:+ %%%  %%%   %%%  %%%    %%% %%%
--    +%+%%%+    +%+%%%+%     +%++:  +%+  %%%  +%+ +%+%%%+%++: +%+%%%+%+  +%+%%%+%++
--   +%+  +%+   +%+           +%+   +%+       +%+ +%+     +%+ +%+               +%+
--  %+%   %+%  %+%           %+%   %+%       %+% %+%     %+% %+%        %+%    %+%
-- %%%    %%% %%%%%%%%%%    %%%   %%%       %%% %%%     %%% %%%         %%%%%%%%



local map = vim.api.nvim_set_keymap
local opts = { noremap = true, silent = true }



-- # Save and source file
map('n', '<leader>x', ':w<CR> :source %<CR>', opts)

-- # NvimTree
map('n', '<leader>to', ':NvimTreeToggle<cr>', opts)


-- map('n', '<expr> k (v:count > 5 ? "m'" . v:count : "") . 'k'
-- map('n', '<expr> k (v:count > 5 ? "m'" . v:count : "") . 'k'
-- map('n', '<expr> j (v:count > 5 ? "m'" . v:count : "") . 'j'

-- vnoremap J :m '>+1<CR>gv=gv
-- vnoremap K :m '<-2<CR>gv=gv
-- " map('i', '<C-j> <esc>:m .+1<CR>==
-- " map('i', '<C-j> <esc>:m .+1<CR>==
-- -- map('n', '<leader>k <esc>:m .-2<CR>==
-- -- map('n', '<leader>j <esc>:m .+1<CR>==



-- -- LSP Stuff

-- map('n', 'gd' ':LspDefinition<CR>
-- map('n', '<leader>k' '<cmd>lua vim.lsp.diagnostic.goto_prev()<CR>'
-- map('n', '<leader>j' '<cmd>lua vim.lsp.diagnostic.goto_next()<CR>'
-- map('n', '<leader>e' '<cmd>lua vim.lsp.nexterror()<CR>'
-- map('n', '<leader>cn' '<cmd>lua vim.lsp.change()<CR>'
-- map('n', '<leader>cl' '<cmd>lua vim.lsp.diagnostic.clear()<CR>'
-- map('n', '<leader>w' '<cmd>lua vim.lsp.workspace_symbol.All()<CR>'
-- map('n', '<leader>ws' '<cmd>lua require'telescope'.lsp_workspace_symbols()<CR>'

-- lua require'telescope.builtin'.lsp_workspace_symbols()

-- -- LSPSaga

-- -- lsp provider to find the cursor word definition and reference
-- map('n', '<silent> gh <cmd>lua require'lspsaga.provider'.lsp_finder()<CR>'
-- -- or use command LspSagaFinder
-- -- map('n', '<silent> gh :Lspsaga lsp_finder<CR>
-- -- code action
-- map('n', '<silent><leader>ca <cmd>lua require('lspsaga.codeaction').code_action()<CR>'
-- vnoremap <silent><leader>ca :<C-U>lua require('lspsaga.codeaction').range_code_action()<CR>'
-- -- or use command
-- -- map('n', '<silent><leader>ca :Lspsaga code_action<CR>'
-- -- vnoremap <silent><leader>ca :<C-U>Lspsaga range_code_action<CR>'
-- -- show hover doc
-- map('n', '<silent> K <cmd>lua require('lspsaga.hover').render_hover_doc()<CR>
-- -- or use command
-- -- map('n', '<silent>K :Lspsaga hover_doc<CR>
-- -- scroll down hover doc or scroll in definition preview
-- map('n', '<silent> <C-f> <cmd>lua require('lspsaga.action').smart_scroll_with_saga(1)<CR>
-- -- scroll up hover doc
-- map('n', '<silent> <C-b> <cmd>lua require('lspsaga.action').smart_scroll_with_saga(-1)<CR>
-- -- show signature help
-- map('n', '<silent> gs <cmd>lua require('lspsaga.signaturehelp').signature_help()<CR>
-- -- or command
-- -- map('n', '<silent> gs :Lspsaga signature_help<CR>
-- -- rename
-- map('n', '<silent>gr <cmd>lua require('lspsaga.rename').rename()<CR>
-- -- or command
-- -- map('n', '<silent>gr :Lspsaga rename<CR>
-- -- close rename win use <C-c> in insert mode or `q` in noremal mode or `:q`
-- -- preview definition
-- map('n', '<silent> gd <cmd>lua require'lspsaga.provider'.preview_definition()<CR>
-- -- or use command
-- -- map('n', '<silent> gd :Lspsaga preview_definition<CR>
-- -- show
-- map('n', '<silent><leader>cd <cmd>lua require'lspsaga.diagnostic'.show_line_diagnostics()<CR>

-- map('n', '<silent> <leader>cd :Lspsaga show_line_diagnostics<CR>
-- -- only show diagnostic if cursor is over the area
-- map('n', '<silent><leader>cc <cmd>lua require'lspsaga.diagnostic'.show_cursor_diagnostics()<CR>

-- -- jump diagnostic
-- map('n', '<silent> [e <cmd>lua require'lspsaga.diagnostic'.lsp_jump_diagnostic_prev()<CR>
-- map('n', '<silent> ]e <cmd>lua require'lspsaga.diagnostic'.lsp_jump_diagnostic_next()<CR>
-- -- or use command
-- -- map('n', '<silent> [e :Lspsaga diagnostic_jump_next<CR>
-- -- map('n', '<silent> ]e :Lspsaga diagnostic_jump_prev<CR>
-- -- float terminal also you can pass the cli command in open_float_terminal function
-- map('n', '<silent> <A-d> <cmd>lua require('lspsaga.floaterm').open_float_terminal()<CR> -- or open_float_terminal('lazygit')<CR>
-- tnoremap <silent> <A-d> <C-\><C-n>:lua require('lspsaga.floaterm').close_float_terminal()<CR>
-- -- or use command
-- map('n', '<silent> <A-d> :Lspsaga open_floaterm<CR>
-- tnoremap <silent> <A-d> <C-\><C-n>:Lspsaga close_floaterm<CR>

-- -- can use smart_scroll_with_saga to scroll
-- -- and you also can use smart_scroll_with_saga to scroll in signature help win

-- -- map('n', '<C-G> <C-B>



-- -- TMUX STUFF
-- noremap <leader>tn :!tmux send-keys -t testing cl Enter mix\ test Enter<CR><CR>

-- -- Commit Shortcuts.  Currently shelling out to the testing shell.
-- map('n', '<leader>gw :!tmux send-keys -t testing ./wip Enter<CR><CR>
-- map('n', '<leader>gc :!tmux send-keys -t testing ./commit Enter<CR><CR>
-- map('n', '<leader>gp :!tmux send-keys -t testing ./production.sh Enter<CR><CR>

-- -- Prime Sepcials
-- -- map('n', '<C-k> :bnext<CR>
-- -- map('n', '<C-j> :bprev<CR>

-- map <leader>gb :call VimuxRunCommand("clear; go build .")<CR>

-- let mapleader = "\<Space>"

-- -- Easier Escaping
-- map('i', 'jj <Esc>
-- map('i', 'kj <Esc>
-- map('i', 'jk <Esc>


-- -- " REGISTERS I use often
-- -- map('i', '!a <esc>@ai
-- -- map('i', '!c <esc>@ci
-- -- map('i', '!d <esc>@di
-- -- map('i', '!e <esc>@ei
-- -- map('i', '!g <esc>@gi
-- -- map('i', '!t <esc>@ti
-- -- map('i', '!s <esc>@si
-- -- map('i', '!w <esc>@wi

-- -- map('n', '<esc> :noh<return><esc>

-- -- Move Split Panes with more Vimlike Motions
-- -- map('n', '<C-H> <C-W><C-H>
-- -- map('n', '<C-J> <C-W><C-J>
-- -- map('n', '<C-K> <C-W><C-K>
-- -- map('n', '<C-L> <C-W><C-L>

-- -- Split to a Vertical split easily
-- -- map('n', '<leader>pv :wincmd v<bar> :e . <bar> :vertical resize 30<CR>

-- map('n', '<leader>pv :wincmd v<bar> :e %:h <bar><CR>

-- -- map('i', 'zd <esc>!!date<enter>idate: <esc>jjj
-- -- map('n', '<leader>d <esc>!!date<enter>idate: <esc>

-- -- Disable Arrow keys in Escape mode
-- map <up> <nop>
-- map <down> <nop>
-- map <left> <nop>
-- map <right> <nop>

-- -- Disable Arrow keys in Insert mode
-- imap <up> <nop>
-- imap <down> <nop>
-- imap <left> <nop>
-- imap <right> <nop>


-- -- insert line below
-- map('i', '<Shift><Space> o<esc>

-- -- map('n', '<silent> <c-]> <cmd>lua vim.lsp.buf.definition()<CR>
-- -- map('n', '<silent> K     <cmd>lua vim.lsp.buf.hover()<CR>
-- -- map('n', '<silent> H     <cmd>lua vim.lsp.buf.hover()<CR>
-- -- map('n', '<silent> gD    <cmd>lua vim.lsp.buf.implementation()<CR>
-- -- map('n', '<silent> <c-s> <cmd>lua vim.lsp.buf.signature_help()<CR>
-- -- map('n', '<silent> 1gD   <cmd>lua vim.lsp.buf.type_definition()<CR>
-- -- map('n', '<silent> gr    <cmd>lua vim.lsp.buf.references()<CR>
-- -- map('n', '<silent> g0    <cmd>lua vim.lsp.buf.document_symbol()<CR>
-- -- map('n', '<silent> gW    <cmd>lua vim.lsp.buf.workspace_symbol()<CR>
-- -- " map('n', 'gd    <cmd>lua vim.lsp.buf.declaration()<CR>
-- -- map('n', '<silent> gd    <cmd>lua vim.lsp.buf.declaration()<CR>
-- -- map('n', '<silent>gd    <cmd>lua vim.lsp.buf.definition()<CR>


-- -- map('n', '<Leader>f :lua require'telescope.builtin'.find_files(require('telescope.themes').get_dropdown({ winblend = 10 }))<cr>


-- -- Telescope
-- map('n', '<leader>f :lua require'telescope.builtin'.find_files(require('telescope.themes').get_dropdown({ results_width=0.8}))<cr>
-- map('n', '<silent>gr    <cmd>lua require'telescope.builtin'.lsp_references{}<CR>
-- -- map('n', '<silent><C-g> <cmd>lua require'telescope.builtin'.live_grep{}<CR>
-- -- map('n', '<silent><C-p> <cmd>lua require'telescope.builtin'.git_files{}<CR>
-- map('n', '<silent><leader>tg <cmd>lua require'telescope.builtin'.live_grep{}<CR>
-- map('n', '<silent><leader>tp <cmd>lua require'telescope.builtin'.git_files{}<CR>
-- map('n', '<silent><leader>tm <cmd>lua require'telescope.builtin'.marks{}<CR>
-- -- Not really using this yet, but might in the future
-- map('n', '<silent><leader>qf :lua require'telescope.builtin'.quickfix{}<CR>
-- map('n', '<silent><leader>q :lua MakeAndPopulateQFList()<CR>
-- map('n', '<silent><leader>ls :lua require'telescope.builtin'.loclist{}<CR>

-- -- require('telescope.themes').get_dropdown({})

-- -- We learned writing this, but no sure if it's useful
-- -- map('n', '<silent><leader>cf <cmd>lua require('begin.tele').cfwd()<CR>

-- -- Seems useful, but not using yet
-- map('n', '<silent><leader>ch <cmd>lua require('telescope.builtin').command_history{}<CR>

-- -- This works sporadically
-- map('n', '<silent><leader>fz <cmd>lua require('telescope.builtin').current_buffer_fuzzy_find{}<CR>


-- -- Useful in searching in our vim config as it gets more split up
-- map('n', '<silent><leader>vv <cmd>lua require'telescope.builtin'.find_files{ cwd = --~/.config/dotfiles/nvim" }<CR>

-- -- For the astronaut in all of us
-- map('n', '<silent><leader>pl <cmd>lua require('telescope.builtin').planets{show_pluto=true}<CR>

-- -- This doesn't work yet
-- -- map('n', '<silent><leader>ss <cmd>lua require('begin.tele').spellcheck()<CR>

-- -- Lazy Git with Floatterm
-- map('n', '<silent><leader>lg :FloatermNew lazygit<CR>

-- -- Ranger with Floatterm
-- map('n', '<silent><leader>rr :FloatermNew "height=0.9 "width=0.9 ranger<CR>

-- -- Color Picker
-- map('n', '<silent><leader>C :VCoolor<CR>

-- -- Vim-Signature Toggle for showing Marks
-- -- map('n', '<silent><leader>tm :SignatureToggle<CR>

-- -- vim-trailing-whitespace
-- noremap <silent><leader>ff :FixWhitespace<cr>

-- -- Toggle Folding
-- -- TODO: Make this better
-- noremap <silent><leader>fo :set fdm=indent<CR>
-- noremap <silent><leader>fi zR; :set fdm=manual<CR>

-- -- To Focus on the Current Section of the Code
-- noremap <silent><leader>gg :Goyo<cr>

-- -- Quick flipping of colorschemes to a Random one
-- -- noremap <silent><leader>jl :!wal --theme random_dark &<cr>

-- -- Use Tabularize to line up things
-- noremap <silent><leader>ta  :Tabularize/

-- -- Convert a Youtube Link to a Markdown link,
-- -- pulling the title with youtube-dl
-- map('n', '<silent><leader>ll :call MdLink()<cr>

-- -- Create a Gist of the Selected section,
-- -- and post it to Twitch Chat
-- vnoremap <silent><leader>gi :call GistAndPost(mode())<cr>

-- -- Use Vim spellcheck and chose the first presented option
-- -- for the word under the cursor
-- noremap <silent><leader>s 1z=e

-- -- Navigate out to current directory
-- -- More likely to use -, instead of this
-- -- TODO: figure out if -, is from dervish
-- -- noremap <silent><leader>e :edit %:h<cr>

-- -- Toggle a cursor for focusing on the cursor
-- -- Twitch chat hates it
-- map('n', '<silent><leader>H :set cursorline! cursorcolumn!<CR>
-- -- map('n', '<leader>H :set cursorline! cursorcolumn!<CR>
-- map('n', '<silent><leader>h :set cursorline!<CR>

-- -- Built in Vim Mappings
-- map('n', '<silent><leader>" viw<esc>a"<esc>bi"<esc>lel

-- -- Toggling Line Numbers on
-- map('n', '<silent><leader>no :set rnu!<CR>
-- map('n', '<silent><leader>ni :set nu!<CR>

-- -- Faster saving
-- -- ...although maybe this should change
-- noremap ,, <esc>:w!<cr>

-- -- Focus and redistribute split windows
-- noremap ff :resize 100 <CR> <BAR> :vertical resize 220<CR>
-- noremap fm <C-w>=

-- -- Faster Vimrc opening
-- map('n', '<silent><leader>ev :vs $MYVIMRC<CR>

-- -- Quick reformatting of json
-- -- noremap <silent><leader>jq :%!python -m json.tool<cr>

-- -- Quick reloading .vimrc
-- noremap <silent><leader>rc :source ~/.config/nvim/init.vim<cr>

-- -- Surround a word in quotes
-- -- I think I should instead use vim-surround more
-- vnoremap <leader>" <esc>m`'<i"<esc>A"<esc>``<CR>

-- -- Maybe consolidate into one
-- noremap <leader>pk :set nopaste<cr>
-- -- noremap <leader>pp :set paste<cr>
-- noremap <leader>pp :call PasteIt()<CR>

-- -- press <Tab> to expand or jump in a snippet. These can also be mapped separately
-- -- via <Plug>luasnip-expand-snippet and <Plug>luasnip-jump-next.
-- imap <silent><expr> <Tab> luasnip#expand_or_jumpable() ? '<Plug>luasnip-expand-or-jump' : '<Tab>' 
-- -- -1 for jumping backwards.
-- map('i', '<silent> <S-Tab> <cmd>lua require'luasnip'.jump(-1)<Cr>

-- snoremap <silent> <Tab> <cmd>lua require('luasnip').jump(1)<Cr>
-- snoremap <silent> <S-Tab> <cmd>lua require('luasnip').jump(-1)<Cr>

-- -- For changing choices in choiceNodes (not strictly necessary for a basic setup).
-- imap <silent><expr> <C-E> luasnip#choice_active() ? '<Plug>luasnip-next-choice' : '<C-E>'
-- smap <silent><expr> <C-E> luasnip#choice_active() ? '<Plug>luasnip-next-choice' : '<C-E>'
