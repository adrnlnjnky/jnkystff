defmodule PasswordManager.GetPassword do
  def which_pass do
    name_raw = IO.gets("What name do you need a password for?: ")
    name = String.replace(name_raw, "\n", "")
    get_pass(name)
  end

  defp get_pass(name) do
    file = Application.get_env(:password_manager, :file_loc)
    list = prepare_list(file, name)

    case list do
      [] ->
        not_found(name)
      _ ->
        return_pass(list)
    end
  end

  defp prepare_list(file, name) do
    file
    |> File.read!()
    |> String.split("\n", trim: true)
    |> Enum.map(&String.split(&1, ",", trim: true))
    |> Enum.map(&get_by_name(&1, name))
    |> Enum.reject(fn x -> x == nil end)
    |> List.flatten()
  end

  defp return_pass(list) do
    password =
      list
      |> decode_pass()

    pretty_pass =
      IO.ANSI.blue() <>
        IO.ANSI.bright() <>
        " -->  " <>
        password <>
        "  <-- \n" <>
        IO.ANSI.reset()

    answer =
      list
      |> List.replace_at(1, " #{pretty_pass} ")
      |> List.insert_at(-2, " | ")

    IO.puts("")
    IO.puts(answer)
    IO.puts("")
  end

  defp not_found(name) do
    red_name = IO.ANSI.red() <> name <> IO.ANSI.reset()

    not_found =
      IO.ANSI.framed() <>
        "No password found for: #{red_name}" <>
        IO.ANSI.reset()

    IO.puts("")
    IO.puts(not_found)
    IO.puts("")
  end

  defp get_by_name([first | _rest] = list, name) when first == name, do: list
  defp get_by_name(_list, _name), do: nil

  defp decode_pass(password) do
    password
    |> Enum.at(1)
    |> Base.decode64!()
  end
end

# PasswordManager.GetPassword.which_pass()
