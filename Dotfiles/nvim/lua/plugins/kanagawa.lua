--##############################################################################
--##############################################################################
--##                                                                         ###
--##                            NVIM CONFIGURATION:                          ###
--##                                 KANAGAWA                                ###
--##                         Colorscheme from nvimdev                        ###
--##                                                                         ###
--##############################################################################
--##############################################################################

return {
  "rebelot/kanagawa.nvim",
  opts = {
    compile = false,
    undercurl = true,
    commentStyle = { italic = true },

    keywordStyle = { italic = true },
    statementStyle = { bold = true },
    typeStyle = {},
    transparent = true,
    dimInactive = true,
    terminalColors = true,
    colors = {
      palette = {},
      theme = {
        wave = {},
        lotus = {},
        dragon = {},
        all = {
          ui = {
            bg_gutter = "none",
          },
        },
      },
    },
    overrides = function(colors) -- add/modify highlights
      local theme = colors.theme
      return {
        -- NormalFloat = { bg = "none" },
        -- FloatBoarder = { bg = "none" },
        -- FloatTitle = { bg = "none" },
        -- NormalDark = { fg = theme.ui.fg_dim, bg = theme.ui.bg_m3 },
        -- LazyNormal = { bg = theme.ui.bg_m3, fg = theme.ui.fg_dim },
        -- MasonNormal = { bg = theme.ui.bg_m3, fg = theme.ui.fg_dim },

        -- TELESCOPE OPTIONS
        TelescopeTitle = { fg = theme.ui.special, bold = true },
        TelescopePromptNormal = { bg = theme.ui.bg_p1 },
        TelescopePromptBorder = { fg = theme.ui.bg_p1, bg = theme.ui.bg_p1 },
        TelescopeResultsNormal = { fg = theme.ui.fg_dim, bg = theme.ui.bg_m1 },
        TelescopeResultsBorder = { fg = theme.ui.bg_m1, bg = theme.ui.bg_m1 },
        TelescopePreviewNormal = { fg = theme.ui.bg_dim },
        TelescopePreviewBorder = { fg = theme.ui.bg_dim, bg = theme.ui.bg_dim },
        Pmenu = { fg = theme.ui.shade0, bg = theme.ui.bg_p1 }, -- add `blend = vim.o.pumblend` to enable transparency
        PmenuSel = { fg = "NONE", bg = theme.ui.bg_p2 }, -- add `blend = vim.o.pumblend` to enable transparency
        PmenuSbar = { bg = theme.ui.bg_m1 }, -- add `blend = vim.o.pumblend` to enable transparency
        PmenuThumb = { bg = theme.ui.bg_p2 }, -- add `blend = vim.o.pumblend` to enable transparency
      }
    end,
    theme = "wave",
    background = {
      dark = "wave",
      light = "lotus",
    },
    keys = {
      { "<leader>wckw", ":colorscheme kanagawa-wave<CR>", desc = "Kanagawa Wave" },
      { "<leader>wckd", ":colorscheme kanagawa-dragon<CR>", desc = "Kanagawa Dragon" },
      { "<leader>wckl", ":colorscheme kanagawa-lotus<CR>", desc = "Kanagawa Night" },
    },
  },
}
