!#/bin/sh

cd

# upgrade and install dependencies (this is gonna block on a password request)
sudo apt upgrade -y
sudo apt install autoconf bison build-essential libssl-dev libyaml-dev libreadline-dev zlib1g-dev libncurses-dev
libffi-dev libgdbm-dev

# Install asdf version control
sudo apt install git curl
git clone https://github.com/asdf-vm/asdf.git ~/.asdf

echo .$HOME/.asdf/asdf.sh >> ~/.profile
echo .$HOME/.asdf/completions/asdf.bash >> ~/.profile

# echo PATH=$PATH:$HOME/.asdf/asdf.sh >> ~/.profile
# echo PATH=$PATH:$HOME/.asdf/completions/asdf.bash >> ~/.profile
source ~/.profile

# Install neovim erlang and elixir (check the versions)
asdf plugin-add neovim
asdf install neovim nightly
asdf shell neovim nightly
asdf global neovim nightly

asdf plugin-add erlang
asdf install erlang 24.3.4
asdf shell erlang 24.3.4
asdf global erlang 24.3.4

asdf plugin-add elixir
asdf install elixir master-otp-24
asdf shell elixir master-otp-24
asdf global elixir master-otp-24


# clone the dotfiles and scripts and move the nvim config files to their home
mkdir .config
git clone https://gitlab.com/adrnlnjnky/jnkystff.git
mv ~/jnkystff/dotfiles/nvim .config/

# install Packer (You'll need to run :PackerInstall -> possibly twice  -> then use PackerSync
git clone --depth 1 https://github.com/wbthomason/packer.nivm ~/.local/share/nvim/site/pack/packer/start/packer.nvim


# add a few convenience features to the bashrc
echo 'alias v='nvim' >> ~/.bashrc'
echo 'alias cl='ll -lF' >> ~/.bashrc'
echo 'alias cla='ll -alF' >> ~/.bashrc'

# add elixir lsp to path
# echo PATH=$PATH:~/.local/share/vim-lsp-settings/servers/elixir-ls >> ~/.profile
# echo PATH=$PATH:~/.local/share/vim-lsp-settings/servers/ >> ~/.profile

source ~/.profile
source ~/.bashrc








