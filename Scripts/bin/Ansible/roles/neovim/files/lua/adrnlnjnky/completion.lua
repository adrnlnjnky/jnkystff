vim.opt.completeopt = "menuone,noselect"

local on_attach_vim = function(client)
	require'completion'.on_attach(client)
end

vim.g.completion_chain_complete_list = {
    default = {
      {complete_items = {"lsp", "buffers", "snippet", "ts"}},
      {mode = "file"},
      {mode = '<c-p>'},
      {mode = '<c-n>'}
    },
    go = {
      {complete_items = {"lsp", "snippet", "ts"}},
      {mode = "file"},
      {mode = '<c-p>'},
      {mode = '<c-n>'}
    },
    markdown = {
      {complete_items = {"buffers"}},
      {mode = "file"},
      {mode = '<c-p>'},
      {mode = '<c-n>'}
    },
    vim = {
      {complete_items = {"buffers"}},
      {mode = "file"},
      {mode = '<c-p>'},
      {mode = '<c-n>'}
    },
    terraform = {
      {complete_items = {"lsp", "buffers"}},
      {mode = "file"},
      {mode = '<c-p>'},
      {mode = '<c-n>'}
    },
    elixir = {
      {complete_items = {"snippet", "lsp", "buffers"}},
      {mode = "file"},
      {mode = '<c-p>'},
      {mode = '<c-n>'}
    },

}

local lspkind = require "lspkind"
local cmp = require "cmp"

cmp.setup {
	autocomplete = true,
	debug = false,
	min_length = 2,
	preselect = "always",
	throttle_time = 80,
	source_timeout = 200,
	incomplete_delay = 500,
	max_abbr_width = 100,
	max_kind_width = 100,
	max_menu_width = 100,
    -- window = {
          -- bordered()
          -- documentation = "native"
          -- border = 'rounded',
          -- winhightlight = "normalFloat:CompDocumentation,FloatBorder:CompDocumentationBorder",
          -- max_width = 60,
          -- min_width = 30,
          -- max_height = math.floor(vim.o.lines * 0.3),
          -- min_height = 1,
        -- },
        mapping = {
          -- ["<c-y>"] = cmp.mapping.confirm{
            -- behavior = cmp.ConfirmBehavior.Insert,
            -- select = true,
          -- },
        },
	source = {
          { name = "luasnip" },
          { name = "nvim_lsp" },
          { name = "nvim_lua" },
          { name = "buffer", keyword_length = 5 },
          { name = "gh_issues" },
		path = true,
		buffer = true,
		calc = true,
		vsnip = true,
		nvim_lsp = true,
		nvim_lua = true,
		spell = true,
		tags = true,
		treesitter = true
	},
        snippet = {
          expand = function(args)
            require("luasnip").lsp_expand(args.body)
          end,
        },
        formatting = {
          format = lspkind.cmp_format {
            with_text = true,
            menu = {
            buffer = "[buf]",
            nvim_lsp = "[LSP]",
            nvim_lua = "[api]",
            path = "[path]",
            luasnip = "[snip]",
            gh_issues = "[issues]",
            },
          },
        },

        experimental = {
          -- native_menu = false,
        },
        ghost_text = true,
      }

-- local capabilities = require('cmp_nvim_lsp').update_capabilities(vim.lsp.protocal.make_client_capabilities())
-- require('lspconfig')['<nvim_lsp>'].setup {
	-- capabilities = capabilities
-- }

local capabilities = vim.lsp.protocol.make_client_capabilities()
-- capabilities.textDocument.completion.completionItem.snippet.Support = true



-- require'cmp'.setup {
-- 	sources = {
-- 		{ name = 'cmp_tabnine' },
-- 	},
-- 	formatting = {
-- 		format = function(entry, vim_item)
-- 			vim_item.kind = lspkind.presets.default[vim_item.kind]
-- 			local menu = source_mapping[entry.source.name]
-- 			if entry.source.name == 'cmp_tabnine' then
-- 				if entry.completion_item.data ~= nil and entry.completion_item.data.detail ~= nil then
-- 					menu = entry.completion_item.data.detail .. ' ' .. menu
-- 				end
-- 				vim_item.kind = ''
-- 			end
-- 			vim_item.menu = menu
-- 			return vim_item
-- 		end
-- 	},
-- }

