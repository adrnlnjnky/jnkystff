**Tom Peltier**

\[Email:\](<adrnlnjnky@protonmail.com>[)]()

661-619-5677

**Goal**

Join a passionate and motivated team where I can contribute and grow

**Coding**

@\. Elixir → My favorite language!

#\. Phoenix

#\. LiveView

@\. Linux

\*. The operating system I prefer.

@\. Shell (bash & zsh)

\*. scripting simple tasks just saves time and brain space.

@\. Go

\*. I really enjoy this simple and powerful language

@\. Git

@\. Database SQL

Postgres

Mysql

@\. Python

@\. Lua

\*. Python is everywhere so I had to learn it.

@\. CSS

#\. Tailwind

#\. Bootstrap

\*. Gotta be able to style those web pages1

@\. HTML

\*. Is it even an option not to know this language?

\## Also (things I\'m skilled with)

@\. Windows

@\. Spreadsheets (Excel)

@\. Word processors

@\. Networking

**Links:**

**Blog**: https://ecstatic-swirles-d8e8ec.netlify.app/

**GitLab** https://www.gitlab.com/adrnlnjnky

**GitHub** https://www.github.com/adrnlnjnky

**Linkedin** https://www.linkedin.com/li/tompeltierii

**Work History:**

**\[Mountain and River Adventures\](MTNRiver.com)**

\*Guide Manager\* March 2016 -- March 2022

Accomplishments:

\*. Successfully drove our client reviews to solid 5 stars.

\*. Brought food costs down by 12%.

#\. This was accomplished through leveraging simple technology.

(spreadsheets and python scripts)

\*. Automated the payroll system

Duties:

\*. Guide Recruiting

\*. Guide Training (I think the hardest thing to teach is decision
making)

#\. Reading and navigating whitewater

#\. Swift water rescue

#\. Boatman skills

#\. Knot tying

#\. Guide Person-ability

#\. Client interaction

#\. Cooking

\*. Work scheduling for

#\. Guides

#\. Drivers

#\. Food service personnel

#\. Support staff

\*. Meetings

#\. Staff

#\. Weekly management meetings

#\. Morning Team Meetings with guides

#\. Debriefs after trips

#\. Client

#\. Safety Briefings

#\. Greeting and Orientation Meetings

#\. Award Ceremonie

\*. Create & Run fleet maintenance Program

#\. when I arrived fleet maintenance was fix it when it broke. The
company now has a functioning maintenance

program.

\*. Payroll Generation and Oversight

\*. Automate the payroll system

#\. once again.. Everything was done by hand even though the software
sweet offered time tacking and software. I implemented the time tracking
software for the company and worked with accounting to make it all
happen smoothly.

\*. Build in-house web application for Warehouse tasks and Guide
information

\*. first in Go and then in Elixir

\$. this was a fun little project with chores, to-dos in checklists as
well as an online copy of our schedule

\*. Supervise Guide Post-trip (debrief) meetings

\*. Conflict resolution

\*. Update Guide Manual regularly

\*. Any other responsibility that gets thrown at the side of my barn.

\*. Lead by being a good example

**\[White Water Voyages\](www.whitewatervoyages.com)2009 - 2016**

Mentor River Guide

\*. Lead Teams of guides and clients on wilderness river trips

#\. Logistics for Transportation, Equipment, and Food

\*. Train guides

\*. Run Meetings

#\. I successfully ran meeting to inspire and excite our staff to a five
star reviews.

\*. Team Leader

\*. Lead by being a good example

**\[Kern River Outfitters\](Kern River Outfitters) 2007 - 2008**

Mentor River Guide

\*. Lead Teams of guides and clients on wilderness river trips

#\. Logistics for Transportation, Equipment, and Food

\*. Train guides

\*. Run Meetings

#\. I successfully ran meeting to inspire and excite our staff to a five
star reviews.

\*. Team Leader

\*. Lead by being a good example

**\[Kern River Tours\](Kern River Tours) Seasons march 2005 - 2007**

Senior River Guide 2005 -- 2007

\*. Lead Teams of guides and clients on wilderness river trips

#\. Logistics for Transportation, Equipment, and Food

\*. Train guides

\*. Run Meetings

#\. I successfully ran meeting to inspire and excite our staff to a five
star reviews.

\*. Team Leader

\*. Lead by being a good example

**\[United States Forrest Service\](United States Forrest Service)**

Summer season 1999

title: Forrest Ranger

#\. Maintain Forrest Service Presence on the river

#\. Interact with the public

#\. Interact with the permit holders

#\. Write the Policies and Procedures for the River Ranger Position

a\. During my time here I co-authored the manual for standards and
practices of a ranger for the Kern River.

**\[Chuck Richards Whitewater\](Chuckrichards.com) Seasons from 1993
thru 1998**

Guide to Senior Guide

\*. Lead Teams of guides and clients on wilderness river trips

#\. Logistics for Transportation, Equipment, and Food

\*. Train guides

\*. Run Meetings

#\. I successfully ran meeting to inspire and excite our staff to a five
star reviews.

\*. Team Leader

-   \*. Lead by being a good example

**\## Off River Jobs**

Golden Automotive -- Aug 2000 -- March 2005

\*. Maintain Network (Windows NT)

\*. Service Writer

Ashe Electrical - Aug 2013 - spring 2015

electrical work

(Company moved to Tennessee)

Skinny Guy Plumbing - Aug 2008 - Spring 2013

plumbing work

(Owner has since retired and moved away)

Red Bud Construction - Sept 2010 - Spring 2015

finish carpentry

tile

water proofing

**\# Our Company**

**\## Kern River Essentials**

Job Title: Co-owner

What Kern River Essentials offer:

\*. Home Automation

#\. System design

We like to use Raspberry Pi's to build cameras and such.

#\. System installation

\*. Construction Projects

\*. Bees wax based creams and lotions
