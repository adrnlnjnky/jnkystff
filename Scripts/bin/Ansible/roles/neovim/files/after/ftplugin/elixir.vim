" g:elixir_showerror 1
" g:elixir_docpreview 1
" g:elixir_maxpreviews 6


nnoremap <leader>fm :w && !mix format<CR><CR>

nnoremap gD :LspDeclaration<CR>
nnoremap gd :LspDefinition<CR>
nnoremap <leader>k <cmd>lua vim.lsp.diagnostic.goto_prev()<CR>
nnoremap <leader>j <cmd>lua vim.lsp.diagnostic.goto_next()<CR>
nnoremap <leader>e <cmd>lua vim.lsp.nexterror()<CR>
nnoremap <leader>K <cmd>lua vim.lsp.hover()<CR>
nnoremap <leader>cl <cmd>lua vim.lsp.diagnostic.clear()<CR>
nnoremap <leader>rn :LspRename<CR>
" nnoremap <leader>rn <cmd>lua vim.lsp.rename()<CR>
nnoremap <leader>w <cmd>lua vim.lsp.workspace_symbol.All()<CR>
nnoremap <leader>ws <cmd>lua require'telescope'.lsp_workspace_symbols()<CR>

" Testing
noremap <leader>tl :!tmux send-keys <leader>cl Enter mix\ test enter<cr><cr>
noremap <leader>tn :!tmux send-keys -t TESTING cl Enter mix\ test Enter<CR><CR>
noremap <leader>rs :!tmux send-keys -t TESTING cl Enter mix\ run\ priv/repo/seeds.exs Enter<CR><CR>

" inoremap defmodule defmodule name do<enter>end<esc>keebcw
" inoremap resourcex resource "",<esc>hhc
" nnoremap <leader>dd i@doc<space><space><space>"""<enter><enter><enter><enter>"""<enter>def zsnotwordk do<enter><enter>end<esc>5ki<tab><esc>
" inoremap docc @doc<space><space><space>"""<enter><enter>"""<enter>def  do<enter><enter>end<esc>5ki<tab><tab>
" inoremap moduledoc @moduledoc<space>"""<enter><enter>"""<enter>def  do<enter><enter>end<esc>4ki<tab><tab>
" inoremap mc @moduledoc<space>"""<enter><enter>"""<enter>def  do<enter><enter>end<esc>4ki<tab><tab>

" inoremap do do<enter><enter>end<esc>3ko

" inoremap deff def name do<enter>end<esc>kebcw
" inoremap defh @impl true<enter>def handle_%{"id" => id}, socket) do<enter>{:noreply, socket}<enter>end<esc>2k8li
" inoremap defp defp name do<enter>end<esc>keebcw
" inoremap withh with<enter>end<esc>klli{}<esc>hi
" inoremap casee case name do<enter>end<esc>keebcw
" inoremap iff if name do<enter>else<enter>end<esc>kebcw

" inoremap pipelinex pipeline name do<enter>end<esc>kebcw
" inoremap schemax schema name do<enter>end<esc>keebcw
" inoremap scopex scope name do<enter>end<esc>keebcw


" noremap <leader>pt iIO.put("*********************************************************************************")<CR>IO.inspect()<CR>IO.put("*********************************************************************************")<esc>kh

" autocmd BufWritePre *.exs,*.ex silent :!mix format --check-equivalent %
"

