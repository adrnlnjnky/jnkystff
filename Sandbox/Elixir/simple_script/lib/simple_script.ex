defmodule SimpleScript do
  @moduledoc """
  Documentation for `SimpleScript`.
   Learning how to use bakeware to see if this is a good option for the cli
  tool.
  """
  use Bakeware.Script

  @impl Bakeware.Script
  def main([]) do
    IO.puts "Pass me an argument and I will upcase them if you specify --upcase")
  end

  # def main(args) do
  def main() do
    args
    |> parse_args
    |> response
    |> IO.puts 
    0
  end

  defp parse_args(args) do
    {opts, word, _} =
      args
      |> OptionParser.parse(switches: [upcase: :boolean])
    {opts, List.to_string(word)}
  end

  defp response({opts, word}) do
    if opts[:upcase], do: String.upcase(word), else: word

  end

end
