-- ###############################################################################
-- ###############################################################################
-- ###                                                                         ###
-- ###                            NVIM CONFIGURATION:                          ###
-- ###                                CATPPUCCIN                               ###
-- ###        Colorscheme with some pretty easy to look at colors 4            ###
-- ###                                                                         ###
-- ###############################################################################
-- ###############################################################################

local opts = { mode = "n", noremap = false, nowait = true, silent = true }

return {
  "catppuccin/nvim",
  name = "catppuccin",
  priority = 1000,

  opts = {
    transparent_background = false,
    dim_inactive = {
      enabled = false,
      shade = "dark",
      percentage = 0.15,
    },
    no_italic = false,
    no_bold = false,
    no_underline = false,
  },
  color_overrides = {},
  custom_highlights = {},
  integrations = {
    cmp = true,
    gitsigns = true,
    nvimtgree = true,
    telescope = true,
    notify = false,
    mini = true,
  },
  -- {
  --   WhichKey = { link = "NormalFloat" },
  --   WhichKeyBorder = { link = "FloatBorder" },
  --   WhichKeygroup = { fg = c.blue },
  --   WhichKeySeperator = { fg = c.overlay0 },
  --   WhichKeyDesc = { fg = c.pink },
  --   WhichKeyvalue = { fg = C.overlay0 },
  -- },
}
