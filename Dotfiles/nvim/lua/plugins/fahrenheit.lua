-- ###############################################################################
-- ###############################################################################
-- ###                                                                         ###
-- ###                            NVIM CONFIGURATION:                          ###
-- ###                               COLORSCHEMES                              ###
-- ###                                MINI_HUES                                ###
-- ###                 Purple background with soft colors                      ###
-- ###                                                                         ###
-- ###############################################################################
-- ###############################################################################

return {
  { "echasnovski/mini.hues" },
}
