" -- ###############################################################################
" -- ###############################################################################
" -- ###                                                                         ###
" -- ###                            NVIM CONFIGURATION:                          ###
" -- ###                              COLOR MY PENCIL                            ###
" -- ###                     a fun bit of color for my coding enjoyment          ###
" -- ###                                                                         ###
" -- ###############################################################################
" -- ###############################################################################

fun! ColorMyPencils()
    let g:gruvbox_contrast_dark = 'hard'
    if exists('+termguicolors')
        let &t_8f = "\<Esc>[38;2;%lu;%lu;%lum"
        let &t_8b = "\<Esc>[48;2;%lu;%lu;%lum"
    endif
    let g:gruvbox_invert_selection='0'

    set background=dark
    if has('nvim')
        call luaeval('vim.cmd("colorscheme " .. _A[1])', [g:smoking_ears_colorscheme])
    else
        colorscheme gruvbox8_hard
    endif

    highlight ColorColumn ctermbg=0 guibg=grey
    " highlight background_colour = "#000001",
    hi SignColumn guibg=#7A7BAB
    hi CursorLineNR guibg=None
    highlight Normal guibg=none
    highlight LineNr guifg=#FA7A13
    highlight netrwDir guifg=#F8F9A3
    highlight qfFileName guifg=#CBA75f
    hi TelescopeBorder guifg=#5eacdF
endfun

call ColorMyPencils()


highlight LspDiagnosticsDefaultError guibg=#9A508B
highlight LspDiagnosticsSignError guibg=#FAD544
highlight LspDiagnosticsUnderlineWarn guibg=#B07600

" highlight clear SpellBad
" highlight SpellBad gui=undercurl

" Joker Theme
highlight CursorLine ctermbg=21FF13 guifg=#9BBA7B
highlight CursorColumn ctermbg=9E00FF ctermfg=darkred

" Vim with me
nnoremap <leader>cmp :call ColorMyPencils()<CR>

