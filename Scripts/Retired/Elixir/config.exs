import Config

config(:getpass, GetPass, :file, "~/.secrets/passwords.txt")

# config :get_pass "~/.jnkystff/main/Scripts/bin/Elixir/get_pass.exs"
# config :save_pass "~/.jnkystff/main/Scripts/bin/Elixir/encode_pass.exs"
# config :groceries, Groceries.Mailer, adapter: Swoosh.Adapters.Local

import_config(#{config_env()}.exs)
