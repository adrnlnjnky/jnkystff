--[[

      enrich_config = function(config, on_config)
        local final_config = vim.deepcopy(config)
        final_config.extra_property = 'This got injected by the adapter'
        on_config(final_config)
      end;

--]]

-- Local Job = require "plenary.job"

local dap = require "dap"


dap.adapters.mix_task = {
  type = 'executable',
  command = '/.local/share/vim-lsp-settings/servers/elixier-ls/language_server.sh',
  args = {}
}

dap.configurations.elixir = {
  {
    type = "mix_task",
    name = "mix test",
    task = 'test',
    taskArgs = {"--trace"},
    request = "launch",
    startApps = "true",
    projectDir = "${workspaceFolder}",requireFiles = {
      "test/**/test_helper.exs",
      "test/**/*_test.exs"
    }
  },
}
