--##############################################################################
--##############################################################################
--##                                                                         ###
--##                            NVIM CONFIGURATION:                          ###
--##                             OCEANIC MATERIALS                           ###
--##                         Colorscheme from nvimdev                        ###
--##                                                                         ###
--##############################################################################
--##############################################################################

return {
  "nvimdev/oceanic-material",
  config = function()
    vim.g["oceanic_material_transparent_background"] = 1
    vim.g["oceanic_material_allow_bold"] = 1
    vim.g["oceanic_material_allow_italic"] = 1
    vim.g["oceanic_material_allow_underline"] = 1
    vim.g["oceanic_material_allow_undercurl"] = 1
  end,
}
